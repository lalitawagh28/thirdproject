<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <meta content="width=device-width, initial-scale=1.0" name="viewport">
      <title>Home Page</title>
      <meta content="" name="descriptison">
      <meta content="" name="keywords">
      <meta name="csrf-token" content="{{ csrf_token() }}" />
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Dosis:300,400,500,,600,700,700i|Lato:300,300i,400,400i,700,700i" rel="stylesheet">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
      <link href="{{ url('frontassets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/venobox/venobox.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/line-awesome/css/line-awesome.min.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <link href="{{ url('frontassets/css/style.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/css/style2.css') }}" rel="stylesheet">
   </head>
   <body>
      @include("web.header")
      @include("web.category_slider")
      <div class="container-fluid" style="width: 91%;margin-top: 14px;">
         <div class="row">
            <div class="col-lg-12">
              <!--  <p style="font-size: 14px;"><b>Home</b> &nbsp;<i class="fas fa-chevron-right" style="font-size: 12px;"></i> <span style="color: #41CF2E;">Amul Pasturised Butter</span></p> -->
            </div>
         </div>
      </div>
      <!-- ======= Hero Section ======= -->
      <section class="d-flex align-items-center" style="background-color: white !important;">
         <div class="container-fluid" style="margin-top: -10px;">
            <div class="row">
               <div class="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center">
                  <center>
                  	<input type="hidden" name="getcountdata" id="getcountdata" value="<?php echo count($productdetails);?>">
                     <div class="prodiv98">
                     	<?php 

                      foreach ($productdetails as $key => $value) { 
                     		if($key == 0)
                     		{
                      	?>
                        <img src="{{ url($value->product_image) }}" alt="" id="big<?php echo $key;?>" class="pro_detone">
                       	<?php }else {?>
                       	<img src="{{ url($value->product_image) }}" alt="" id="big<?php echo $key;?>" class="pro_detone" style="display:none;">
                       	<?php } } ?>
                        
                     </div>
                     <br><br>
                     <div class="row" style="width: 85%;">
                     	<?php foreach ($productdetails as $key => $value) { 
                      	?>
                        <div class="col-xl-3 mob_img34">
                           <div class="card shadow s<?php echo $key;?> pro_detcard"  style="border: 2px solid">
                              <img src="{{ url($value->product_image) }}" alt="" id="small<?php echo $key;?>" onclick="getdata('<?php echo $key?>','<?php echo count($productdetails)?>','<?php echo $value->product_id;?>')" class="pro_detimg99">
                           </div>
                           <div class="shadow pro_detshadow"></div>
                        </div>
                        <?php } ?>

                       
                     </div>
                  </center>
               </div>
               <?php 
               if(count($productdetails) > 0)
               {
               $productsinglevarient =  DB::table('product_varient')->where(array('product_id' => $productdetails[0]->product_id))->get();
               	$productvarient = DB::table('product_varient')->where(array('product_id' => $productdetails[0]->product_id))->first();
               ?>
               <div class="col-lg-6 order-2 order-lg-2 hero-img right_prodiv" id="productdetaildata">
                  <h3><?php echo $productdetails[0]->product_name;?> <span style="float: right;font-size: 15px;position: relative;top: 5px; right: 100px;"><i class="fas fa-star" style="color: #37A235"></i> <i class="fas fa-star" style="color: #37A235"></i> <i class="fas fa-star" style="color: #37A235"></i> <i class="far fa-star"></i> <i class="far fa-star"></i></span></h3>
                  <p style="margin-top: 19px;font-size: 15px;"><b>Select Size:</b></p>
                  <select class="select_gram23 shadow-sm" onchange="changeproductdeatils(this)">
                  	<?php foreach($productsinglevarient as $productsinglevarient) { ?>
                     <option value="<?php echo $productsinglevarient->varient_id;?>"><?php echo $productsinglevarient->quantity.''.$productsinglevarient->unit;?></option>
                     
                    <?php }?>
                  </select>
                  <input type="hidden" name="piddata" id="piddata" value="<?php echo $productdetails[0]->product_id;?>">
                  <input type="hidden" name="varientiddata" id="varientiddata" value="<?php echo $productvarient->varient_id;?>">
                  <h3 style="margin-top: 25px;"><b ><span id="pricedata">Rs.<?php echo $productvarient->base_price;?></span></b></h3>
                  <p style="margin-bottom: 10px;font-size: 14px;margin-top: -6px;color: #37A235;"><!-- Extra ₹60 discount --></p>
                  <div class="btn-group btn-group-sm outer_but772new shadow"  style="border-radius: 30px;" role="group" aria-label="...">
                     <button class="inner_one2new" onclick="getminus()"><i class="fas fa-minus inner_one_i2new"></i></button>
                     <button class="inner_two2new"><span class="inner_two_span2new">1</span></button>
                     <button class="inner_three2new" onclick="getplus()"><i class="fas fa-plus inner_three_i2new"></i></button>
                  </div>
                  <?php if(session('userid') != '') { ?>
                  <button class="add_butnew shadow" onclick="addtobag(<?php echo session('userid') ?>)">Add to bag</button>
                  <?php }else {?>
                  	<button class="add_butnew shadow" onclick="addtobag(0)">Add to bag</button>
                  		
                  <?php }?>
                  <h5 style="margin-top: 42px;">Product Description :</h5>
                  <p style="width: 90%;font-size: 13px;margin-top: 13px;color: #A6A6A6"><?php echo $productsinglevarient->description;?></p>
               </div>
               <?php } else {?>
               <h3 style="padding-left:803px;color:red">Product Not Avialable.</h3>
               <?php }?>
            </div>
         </div>
      </section>
      <!-- End Hero -->
      <hr>
      <div class="container-fluid" style="width: 89.5%;margin-top: 22px;">
         <div class="row" style="margin-top: 30px;margin-bottom: -1px;">
            <div class="col-xl-12">
               <center>
                  <h3 >More From This Brand</h3>
               </center>
            </div>
         </div>
         <div class="row">
            <div class="owl-carousel owl-theme" id="owl-two2"  style="transform: translate3d(0px, 7px, 0px);margin-bottom: 30px;">
            	<?php $i = 0;foreach ($product as $key => $value) { ?>
            		
            	
               <div class="col-lg-3 col-md-6 d-flex align-items-stretch item shadow prodetails_card">
                  <div class="member">
                     <div class="member-img">
                        <center><img src="assets/img/d1.jpg" class="img-fluid sel_img23" alt=""></center>
                     </div>
                     <div class="member-info">
                        <h4 class="head_hfour">Walter White Salt</h4>
                        <select class="select_gram">
                           <option value="500gm">500gm</option>
                           <option value="100gm">100gm</option>
                        </select>
                        <h4 class="h4_rupee">Rs. 235</h4>
                        <div class="btn-group btn-group-sm outer_but772" role="group" aria-label="...">
                           <button class="inner_one<?php echo $i;?>" onclick="getminus(<?php echo $i;?>)"><i class="fas fa-minus inner_one_i2"></i></button>
                           <button class="inner_two<?php echo $i;?>"><span class="inner_two_span<?php echo $i;?>">1</span></button>
                           <button class="inner_three<?php echo $i;?>" onclick="getplus(<?php echo $i;?>)"><i class="fas fa-plus inner_three_i2"></i></button>
                        </div>
                        <button class="add_but">Add to bag</button>
                     </div>
                  </div>
               </div>
               <?php $i++;} ?>
               
            </div>
         </div>
      </div>
      <main id="main" class="shadow">
         <!-- ======= About Section ======= -->
         <section id="about" class="about">
            <div class="container" style="margin-top: -50px;margin-bottom: -30px;">
               <div class="row">
                  <div class="col-xl-4 col-lg-4 d-flex justify-content-center align-items-stretch">
                     <div class="icon-box">
                        <div class="icon"><i class="fas fa-hand-holding-usd"></i></div>
                        <h4 class="title"><a href="">Best Price & Offers</a></h4>
                        <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 d-flex justify-content-center align-items-stretch">
                     <div class="icon-box">
                        <div class="icon"><i class="fas fa-inbox"></i></div>
                        <h4 class="title"><a href="">Wide Assorment</a></h4>
                        <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 d-flex justify-content-center align-items-stretch">
                     <div class="icon-box">
                        <div class="icon"><i class="fas fa-rupee-sign"></i></div>
                        <h4 class="title"><a href="">Easy Return</a></h4>
                        <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- End About Section -->
      </main>
      <!-- End #main -->
       @include("web.footer")
      <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
      <!-- Vendor JS Files -->
      <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
      <!-- Vendor JS Files -->
      <script src="{{ url('frontassets/vendor/jquery/jquery.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/php-email-form/validate.js') }}"></script>
      <script src="{{ url('frontassets/vendor/venobox/venobox.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/counterup/counterup.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
      <!-- Template Main JS File -->
      <script src="{{ url('frontassets/js/main.js') }}"></script>
   </body>
</html>
<script>
   $(document).ready(function(){
     $('#owl-one').owlCarousel({
       loop:true,
       margin:10,
       nav:true,
                       
   responsive: {
           0:{
               items:1
           },
           600:{
               items:4
           },
           1000:{
               items:7
           }
       }
   })
      $( ".owl-prev").html('<img src="assets/img/l1.png" height="45" style="margin-left:10px;margin-top:30px;" height="55"  class="imgkl2 shadow">');
      $( ".owl-next").html('<img src="assets/img/r2.png" height="45" style="margin-right:10px;margin-top:30px;" height="55" class="imgkl2 shadow">');
   });
   
   
</script>
<script>
   $(document).ready(function(){
     $('#owl-two2').owlCarousel({
       loop:true,
       margin:10,
       nav:true,
                       
   responsive: {
           0:{
               items:1
           },
           600:{
               items:3
           },
           1000:{
               items:5
           }
       }
   })
     $( "#owl-two2 .owl-nav .owl-prev").html('<img src="assets/img/l1.png" height="45" style="margin-left:10px;margin-top:150px;" height="55"  class="imgkl shadow">');
      $( "#owl-two2 .owl-nav .owl-next").html('<img src="assets/img/r2.png" height="45" style="margin-right:-10px;margin-top:150px;" height="55" class="imgkl shadow">');
   });
   
   
</script>
<style type="text/css">
   .imgkl{
   background-color: white;
   }
   .imgkl:hover
   {
   background: white !important;
   }
   .imgkl2{
   background-color: white;
   }
   .imgkl2:hover
   {
   background: white !important;
   }
</style>
<script>
	 	function  getdata(iddata,count,pid) {
   		for(var i = 0;i <= count; i++)
		{
			if(iddata == i)
			{
 				$("#big"+i).show('slow');
 				$(".s"+i).css('border','2px solid');
			}else
			{
				$("#big"+i).hide('slow');
				$(".s"+i).css('border','1px solid #DFDFDF');
			}
		}

   		$.ajaxSetup({
                     headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     }
         });
         $.ajax({
            url: "{{ route('getproductdata') }}",
            type:"POST",
            data:{pid: pid},
            success: function(result){

               $('#productdetaildata').html(result);
              
            }
         });
   	}
   $(document).ready(function(){
   
   
   });


</script>
<script type="text/javascript">
	function getminus()
   {
   		
      var data = $('.inner_two_span2new').html();
      if(data > 0)
      {
         var minusdata = parseInt(data) - 1;
         $('.inner_two_span2new').html(minusdata);
      }else 
      {
         $('.inner_two_span2new').html(0);
      }
     
   }
   function getplus()
   {
      var data = $('.inner_two_span2new').html();
      
      var plusdata = parseInt(data) + 1;
      $('.inner_two_span2new').html(plusdata);
   }
   function  changeproductdeatils(the) {
   		var varientid = $(the).val();
   		$.ajaxSetup({
                     headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     }
         });
         $.ajax({
            url: "{{ route('getproductdetailschange') }}",
            type:"POST",
            data:{varientid: varientid},
            success: function(result){
            	var obj = JSON.parse(result);
               $('#pricedata').html('Rs.'+obj.price);
               $('#piddata').val(obj.pid);
               $('#varientiddata').val(varientid);
              
            }
         });
   }
   function addtobag(userid)
   {
   		if(userid == 0)
   		{
   			alert('Please Login !!!');
   		}else
   		{
   			var pid = $('#piddata').val();
   			var qty = $('.inner_two_span2new').html();
   			var varient = $('#varientiddata').val();
   			
   			$.ajaxSetup({
                     headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     }
         	});
	         $.ajax({
	            url: "{{ route('addtobag') }}",
	            type:"POST",
	            data:{userid: userid,pid:pid,qty:qty,varient:varient},
	            success: function(result){
	            	if(result == 1)
	            	{
	            		alert('Product added to cart successfully');
	            	}else if(result == 2){
	            		alert('Product allready added to cart ');
	            	}else
	            	{
	            		alert('Product not added to cart');
	            	}

	            }
	         });
   		}
   		
   }
   
</script>