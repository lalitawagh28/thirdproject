<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta content="width=device-width, initial-scale=1.0" name="viewport">
      <title>Home Page</title>
      <meta content="" name="descriptison">
      <meta content="" name="keywords">
      <meta name="csrf-token" content="{{ csrf_token() }}" />
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Dosis:300,400,500,,600,700,700i|Lato:300,300i,400,400i,700,700i" rel="stylesheet">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
      <link href="{{ url('frontassets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/venobox/venobox.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/line-awesome/css/line-awesome.min.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <link href="{{ url('frontassets/css/style.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/css/style2.css') }}" rel="stylesheet">
   </head>
   <body>
       @include("web.header")
      <style type="text/css">
       
         @media only screen and (max-width: 768px) {
         .checkout_cardbody
         {
         width: 100%;margin-left: 0px !important;
         }
         .checkout_input
         {
         max-width:95%;
         }
         .checkout_next
         {
         width: 100%;
         margin-top: 15px;margin-left: 0px;
         }
         .checkout_order
         {
         width: 100%;
         }
         #nono
         {
         min-height: 320px;
         }
         #nono2
         {
         min-height: 80px;
         }
         }
      </style>
      <div class="container-fluid checkout_cont">
         <div class="row">
            <div class="col-xl-3">
                  <div class="card shadow">
                     <h3 style="margin: 15px 0px 35px 18px;font-size: 20px;"><b>My Account</b></h3>
                     <style type="text/css">
                        .myaccount_side
                        {
                        height: 60px;padding-top: 15px;cursor: pointer;
                        }
                        .myaccount_side_p
                        {
                        font-size: 17.5px;margin-left: 20px;font-weight: 500; 
                        }
                     </style>
                     <div id="side1" class="myaccount_side" style="background-color: #4BA345;">
                        <p class="myaccount_side_p" id="para1" style="color: white;"><i class="fas fa-box"></i> &nbsp;My Orders <span style="float: right;"><i class="fas fa-angle-double-right" style="margin-right: 20px;"></i></span></p>
                     </div>
                     <div id="side2" style="background-color: white;" class="myaccount_side">
                        <p class="myaccount_side_p" id="para2" style="color: black;"><i class="fas fa-box"></i> &nbsp;Active Orders <span style="float: right;"><i class="fas fa-angle-double-right" style="margin-right: 20px;"></i></span></p>
                     </div>
                     <div id="side7" style="background-color: white;" class="myaccount_side">
                        <p class="myaccount_side_p" id="para7" style="color: black;"><i class="fa fa-gift"></i> &nbsp;Rewards<span style="float: right;"><i class="fas fa-angle-double-right" style="margin-right: 20px;"></i></span></p>
                     </div>
                     <div id="side8" style="background-color: white;" class="myaccount_side">
                        <p class="myaccount_side_p" id="para8" style="color: black;"><i class="fa fa-wallet"></i> &nbsp;Wallet<span style="float: right;"><i class="fas fa-angle-double-right" style="margin-right: 20px;"></i></span></p>
                     </div>
                     <div id="side3" style="background-color: white;" class="myaccount_side">
                        <p class="myaccount_side_p" id="para3" style="color: black;"><i class="far fa-user-circle"></i> &nbsp;Personal Details <span style="float: right;"><i class="fas fa-angle-double-right" style="margin-right: 20px;"></i></span></p>
                     </div>
                     <div id="side4" style="background-color: white;" class="myaccount_side">
                        <p class="myaccount_side_p" id="para4" style="color: black;"><i class="far fa-address-card"></i> &nbsp;Delivery Address <span style="float: right;"><i class="fas fa-angle-double-right" style="margin-right: 20px;"></i></span></p>
                     </div>
                     <div id="side5" style="background-color: white;" class="myaccount_side">
                        <p class="myaccount_side_p" id="para5" style="color: black;"><i class="fas fa-people-carry"></i> &nbsp;Customer Service <span style="float: right;"><i class="fas fa-angle-double-right" style="margin-right: 20px;"></i></span></p>
                     </div>
                     <br><br>
                  </div>
            </div>
            <div class="col-xl-9 rightacc">
                  <div id="div1" class="card shadow" >
                     <h3 style="margin: 13px 0px 30px 22px;font-size: 22px;"> <i class="fas fa-box" style="color: #4BA345;"></i>&nbsp;My Orders</h3>
                     <center class="saved_mob">
                        <table class="table saved_mob2" style="width: 95%;border: 5px solid #DEE2E6;margin-bottom: 30px;">
                           <thead>
                              <tr >
                                 <th scope="col" style="width: 30%;">Order Placed On:</th>
                                 <th scope="col" style="width: 20%;">Order Amount:</th>
                                <!--  <th scope="col" style="width: 10%;">Items:</th> -->
                                 <th scope="col" style="width: 20%;">Status:</th>
                                 <th scope="col" style="width: 22%;">Action:</th>
                              </tr>
                           </thead>
                           <tbody>
                              <?php foreach($orders as $orders) {?>
                              <tr style="line-height: 40px;">
                                 <th scope="row"> {{date('d F Y',strtotime($orders->order_date))}}</th>
                                 <td>Rs.{{ $orders->total_price}}</td>
                                 <!-- <td>2</td> -->
                                 <td style="color: orange">{{ $orders->order_status}}</td>
                                 <td style="color: #4BA345;cursor: pointer;" data-toggle="modal" data-target="#orderdetails"><i class="fas fa-map-marker-alt" ></i> <span style="font-weight: 500">View Order</span></td>
                              </tr>
                              <?php }?>
                           </tbody>
                        </table>
                     </center>
                  </div>
                  <div class="modal fade" id="orderdetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
                     <div class="modal-dialog modalacc_doc" role="document">
                        <div class="modal-content modalacc_content">
                           <div class="modal-header">
                              <h5 class="modal-title modal_head" id="exampleModalLabel"><i class="fas fa-map-marker-alt" style="color: #F27A35;"></i> View Order</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                           </div>
                           <div class="modal-body" style="padding: 30px 20px 20px 40px">
                              <div class="">
                                 <div class="row">
                                    <div class="col-lg-4 col-md-6">
                                       <p style="font-weight: 500;font-size: 15.5px;"> <i class="fas fa-angle-double-right" style="color: #f27a35;"></i> Delivery Address:</p>
                                       <p class="myaccount_para3">Kuwar Raman Singh</p>
                                       <p class="myaccount_para2">Kuwar Raman Singh</p>
                                       <p class="myaccount_para">Your Local Address</p>
                                       <p class="myaccount_para">Your Address</p>
                                       <p class="myaccount_para">Your Pincode</p>
                                       <p class="myaccount_para">Your State</p>
                                       <p class="myaccount_para">Your Country</p>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                       <p style="font-weight: 500;font-size: 15.5px;"><i class="fas fa-angle-double-right" style="color: #f27a35;"></i>  Payment Info :</p>
                                       <p class="myaccount_para4">Status : <span style="color: #f27a35;">Ready for Dispatch</span></p>
                                       <p class="myaccount_para">Mode : COD</p>
                                       <p style="font-weight: 500;font-size: 15.5px;"><i class="fas fa-angle-double-right" style="color: #f27a35;"></i> Store Name :</p>
                                       <p class="myaccount_para4">R.K trading Company</p>
                                    </div>
                                    <div class="col-lg-4 col-md-12">
                                       <p style="font-weight: 500;font-size: 15.5px;"><i class="fas fa-angle-double-right" style="color: #f27a35;"></i> Order Summary :</p>
                                       <p class="myaccount_para4">Sub-Total : <span style="float: right;">Rs.1,693</span></p>
                                       <p class="myaccount_para4">Delivery Fee <span style="float: right;">Rs.16</span></p>
                                       <p class="myaccount_para4">Express Delivery <span style="float: right;">Rs.0</span></p>
                                       <hr style="border-bottom: 8px solid #F6F6F6">
                                       <p class="myaccount_total">Total <span style="float: right;">Rs.1,712</span></p>
                                       <hr style="border-bottom: 8px solid #F6F6F6">
                                    </div>
                                    <div class="col-xl-12">
                                       <button class="myaccount_cancel">Cancel</button>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div id="div2" class="card shadow" style="display: none;">
                     <h3 style="margin: 13px 0px 30px 22px;font-size: 22px;"> <i class="fas fa-box" style="color: #4BA345;"></i>&nbsp;Active Orders</h3>
                     <center class="saved_mob">
                        <table class="table saved_mob2" style="width: 95%;border: 5px solid #DEE2E6;margin-bottom: 30px;">
                           <thead>
                              <tr >
                                 <th scope="col" style="width: 30%;">Order Placed On:</th>
                                 <th scope="col" style="width: 20%;">Order Amount:</th>
                               
                                 <th scope="col" style="width: 20%;">Status:</th>
                                 <th scope="col" style="width: 22%;">Action:</th>
                              </tr>
                           </thead>
                           <tbody>
                            <?php foreach($activeorder as $ordersdata) {?>
                              <tr style="line-height: 40px;">
                                 <th scope="row"> {{date('d F Y',strtotime($ordersdata->order_date))}}</th>
                                 <td>Rs.{{ $ordersdata->total_price}}</td>
                                 <!-- <td>2</td> -->
                                 <td style="color: orange">{{ $ordersdata->order_status}}</td>
                                 <td style="color: #4BA345;cursor: pointer;" data-toggle="modal" data-target="#orderdetails"><i class="fas fa-map-marker-alt" ></i> <span style="font-weight: 500">View Order</span></td>
                              </tr>
                              <?php }?>
                           </tbody>
                        </table>
                     </center>
                  </div>
                  <div id="div7" class="card shadow" style="display: none;">
                     <h3 style="margin: 13px 0px 30px 22px;font-size: 22px;"> <i class="fas fa-box" style="color: #4BA345;"></i>&nbsp;Reward</h3>
                     <center class="saved_mob">
                       <span>Total Reward Points:</span>
                     </center>
                  </div>
                  <div id="div8" class="card shadow" style="display: none;">
                     <h3 style="margin: 13px 0px 30px 22px;font-size: 22px;"> <i class="fas fa-box" style="color: #4BA345;"></i>&nbsp;Wallet</h3>
                     <center class="saved_mob">
                          <span>Total Wallet Amount:</span>
                     </center>
                  </div>
                  <div id="div3" class="card shadow" style="display: none;">
                     <form method="POST" action="{{ route('updateprofile') }}">
                        {{ csrf_field() }}
                     <h3 style="margin: 13px 0px 30px 22px;font-size: 22px;"> <i class="far fa-user-circle" style="color: #4BA345;"></i>&nbsp;Personal Details</h3>
                     <div class="form-group personal_div">
                        <label for="usr" class="personal_label">Name <i class="fab fa-diaspora" style="color: red;font-size: 11px;"></i></label>
                        <input type="text"  id="usr" name="username" class="personal_input" value="{{$profile->user_name}}">
                     </div>
                    
                     <div class="form-group personal_div">
                        <label for="usr" class="personal_label">Email Address <i class="fab fa-diaspora" style="color: red;font-size: 11px;"></i></label>
                        <input type="text"  id="usr" name="useremail" class="personal_input" value="{{$profile->user_email}}">
                     </div>
                     <div class="form-group personal_div">
                        <label for="usr" class="personal_label">Mobile Number <i class="fab fa-diaspora" style="color: red;font-size: 11px;"></i></label>
                        <input type="text"  id="usr" name="mobilenumber" class="personal_input" value="{{$profile->user_phone}}">
                     </div>
                     
                     <center>
                        <button class="myaccout_submit">Submit</button>
                     </center>
                     </form>
                  </div>
                  <div id="div4" class="card shadow" style="display: none;padding-bottom: 30px;">
                     <h3 style="margin: 13px 0px 30px 22px;font-size: 22px;"> <i class="far fa-address-card" style="color: #4BA345;"></i>&nbsp;Delivery Address <span class="addnew_acc"><a href="edit_address.php"><i class="fas fa-plus"></i> Add New Address</a></span></h3>
                     <div class="container-fluid" style="margin-left: 8px;width: 98.6%;">
                        <div class="row" >
                           <div class="col-xl-6">
                              <div class="card">
                                 <div class="card-header">
                                    <b> Kuwar Raman Singh</b>
                                 </div>
                                 <div class="card-body">
                                    <p style="    font-weight: 400;font-size: 15px;color: #666;margin-top: -10px;">Kuwar Raman Singh</p>
                                    <p class="myaccount_para56">Your Local Address</p>
                                    <p class="myaccount_para56">Your Pincode</p>
                                    <p class="myaccount_para56">Your State,Your Country</p>
                                    <p style="font-weight: 400;font-size: 15px;color: #666;margin-top: 1px;">Your Phone Number</p>
                                    <p class="myaccount_para99">Home</p>
                                 </div>
                              </div>
                              <p class="myaccount_add">Set As Default Address <span style="float: right;"><a href="edit_address.php">Edit Address</a></span> </p>
                           </div>
                           <div class="col-xl-6">
                              <div class="card">
                                 <div class="card-header">
                                    <b> Kuwar Raman Singh</b>
                                 </div>
                                 <div class="card-body">
                                    <p style="    font-weight: 400;font-size: 15px;color: #666;margin-top: -10px;">Kuwar Raman Singh</p>
                                    <p class="myaccount_para56">Your Local Address</p>
                                    <p class="myaccount_para56">Your Pincode</p>
                                    <p class="myaccount_para56">Your State,Your Country</p>
                                    <p style="font-weight: 400;font-size: 15px;color: #666;margin-top: 1px;">Your Phone Number</p>
                                    <p class="myaccount_para99">Home</p>
                                 </div>
                              </div>
                              <p class="myaccount_add">Set As Default Address <span style="float: right;"><a href="edit_address.php">Edit Address</a></span> </p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div id="div5" class="card shadow" style="display: none;">
                     <h3 style="margin: 13px 0px 30px 22px;font-size: 22px;"> <i class="fas fa-people-carry" style="color: #4BA345;"></i>&nbsp;Customer Service</h3>
                     <div class="container-fluid" style="width: 98%;">
                        <div class="row">
                           <div class="col-xl-3" style="border-right: 1px solid #D4D4D4;margin-bottom: 20px;">
                              <p style="font-size: 17px;font-weight: 500;margin-top: 60px;"><i class="fas fa-envelope-open-text"></i> Email / Call</p>
                           </div>
                           <div class="col-xl-9">
                              <center>
                                 <p style="font-size: 19px;font-weight: 500;margin-bottom: 30px;"><i class="fas fa-envelope-open-text"></i> Email</p>
                                 <p style="font-size: 18px;font-weight: 500;">In case of any query, Please contact us to below details</p>
                                 <p style="font-size: 19px;font-weight: 500;">Email will be send to:</p>
                                 <p style="font-size: 19px;font-weight: 500;color: #4BA345;margin-top: -16px;">customercare@xyz.com</p>
                                 <p style="font-size: 19px;font-weight: 500;">OR</p>
                                 <p style="font-size: 19px;font-weight: 500;">Call us at:</p>
                                 <p style="font-size: 19px;font-weight: 500;margin-bottom: 30px;color: #4BA345;margin-top: -16px;">18000800000</p>
                              </center>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
         </div>
      </div>
      <br><br>
       @include("web.footer")
      <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
      <!-- Vendor JS Files -->
      <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
      <!-- Vendor JS Files -->
      <script src="{{ url('frontassets/vendor/jquery/jquery.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/php-email-form/validate.js') }}"></script>
      <script src="{{ url('frontassets/vendor/venobox/venobox.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/counterup/counterup.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
      <!-- Template Main JS File -->
      <script src="{{ url('frontassets/js/main.js') }}"></script>
      <script type="text/javascript">
   $(document).ready(function(){
    
       $('#side1').click(function(){     
           $('#div1').show();  
           $('#div2').hide();  
           $('#div3').hide();  
           $('#div4').hide();  
           $('#div5').hide(); 
           $('#div7').hide(); 
           $('#div8').hide(); 
           $('#side1').css('background-color','#4BA345');    
           $('#side2').css('background-color','white');    
           $('#side3').css('background-color','white');    
           $('#side4').css('background-color','white');    
           $('#side5').css('background-color','white');
            $('#side7').css('background-color','white');
             $('#side8').css('background-color','white'); 
   
           $('#para1').css('color','white');    
           $('#para2').css('color','black');    
           $('#para3').css('color','black');    
           $('#para4').css('color','black');    
           $('#para5').css('color','black');
            $('#para7').css('color','black');
             $('#para8').css('color','black');    
         
       });
   
        $('#side2').click(function(){     
           $('#div1').hide();  
           $('#div2').show();  
           $('#div3').hide();  
           $('#div4').hide();  
           $('#div5').hide();
            $('#div7').hide(); 
           $('#div8').hide();  
           $('#side2').css('background-color','#4BA345');    
           $('#side1').css('background-color','white');    
           $('#side3').css('background-color','white');    
           $('#side4').css('background-color','white');    
           $('#side5').css('background-color','white'); 
             $('#side7').css('background-color','white');
             $('#side8').css('background-color','white'); 
   
           $('#para2').css('color','white');    
           $('#para1').css('color','black');    
           $('#para3').css('color','black');    
           $('#para4').css('color','black');    
           $('#para5').css('color','black');  
            $('#para7').css('color','black');
             $('#para8').css('color','black');     
         
       });
   
         $('#side3').click(function(){     
           $('#div1').hide();  
           $('#div2').hide();  
           $('#div3').show();  
           $('#div4').hide();  
           $('#div5').hide(); 
            $('#div7').hide(); 
           $('#div8').hide(); 
           $('#side3').css('background-color','#4BA345');    
           $('#side2').css('background-color','white');    
           $('#side1').css('background-color','white');    
           $('#side4').css('background-color','white');    
           $('#side5').css('background-color','white');
             $('#side7').css('background-color','white');
             $('#side8').css('background-color','white'); 
   
               $('#para3').css('color','white');    
           $('#para2').css('color','black');    
           $('#para1').css('color','black');    
           $('#para4').css('color','black');    
           $('#para5').css('color','black');   
            $('#para7').css('color','black');
             $('#para8').css('color','black');    
         
       });
   
          $('#side4').click(function(){     
           $('#div1').hide();  
           $('#div2').hide();  
           $('#div3').hide();  
           $('#div4').show();  
           $('#div5').hide(); 
            $('#div7').hide(); 
           $('#div8').hide(); 
           $('#side4').css('background-color','#4BA345');    
           $('#side2').css('background-color','white');    
           $('#side3').css('background-color','white');    
           $('#side1').css('background-color','white');    
           $('#side5').css('background-color','white'); 
             $('#side7').css('background-color','white');
             $('#side8').css('background-color','white'); 
   
               $('#para4').css('color','white');    
           $('#para2').css('color','black');    
           $('#para3').css('color','black');    
           $('#para1').css('color','black');    
           $('#para5').css('color','black'); 
            $('#para7').css('color','black');
             $('#para8').css('color','black');     
         
       });
   
           $('#side5').click(function(){     
           $('#div1').hide();  
           $('#div2').hide();  
           $('#div3').hide();  
           $('#div4').hide();  
           $('#div5').show(); 
            $('#div7').hide(); 
           $('#div8').hide(); 
           $('#side5').css('background-color','#4BA345');    
           $('#side2').css('background-color','white');    
           $('#side3').css('background-color','white');    
           $('#side4').css('background-color','white');    
           $('#side1').css('background-color','white'); 
             $('#side7').css('background-color','white');
             $('#side8').css('background-color','white');   
   
               $('#para5').css('color','white');    
           $('#para2').css('color','black');    
           $('#para3').css('color','black');    
           $('#para4').css('color','black');    
           $('#para1').css('color','black'); 
            $('#para7').css('color','black');
             $('#para8').css('color','black');   
         
       });
           $('#side7').click(function(){     
           $('#div1').hide();  
           $('#div2').hide();  
           $('#div3').hide();  
           $('#div4').hide(); 
            $('#div8').hide(); 
             $('#div5').hide();  
           $('#div7').show(); 
           $('#side7').css('background-color','#4BA345');    
           $('#side2').css('background-color','white');    
           $('#side3').css('background-color','white');    
           $('#side4').css('background-color','white');    
           $('#side1').css('background-color','white');
           $('#side8').css('background-color','white'); 
           $('#side5').css('background-color','white');   
   
               $('#para7').css('color','white');    
           $('#para2').css('color','black');    
           $('#para3').css('color','black');    
           $('#para4').css('color','black');    
           $('#para1').css('color','black'); 
            $('#para5').css('color','black'); 
             $('#para8').css('color','black'); 
         
       });
           $('#side8').click(function(){     
           $('#div1').hide();  
           $('#div2').hide();  
           $('#div3').hide();  
           $('#div4').hide();
           $('#div5').hide();
           $('#div7').hide();  
           $('#div8').show(); 
           $('#side8').css('background-color','#4BA345');    
           $('#side2').css('background-color','white');    
           $('#side3').css('background-color','white');    
           $('#side4').css('background-color','white');    
           $('#side1').css('background-color','white'); 
           $('#side5').css('background-color','white');  
           $('#side7').css('background-color','white');    
   
               $('#para8').css('color','white');    
           $('#para2').css('color','black');    
           $('#para3').css('color','black');    
           $('#para4').css('color','black');    
           $('#para1').css('color','black');
            $('#para5').css('color','black');
             $('#para7').css('color','black'); 
         
       });
     
   });
</script>
   </body>
</html>