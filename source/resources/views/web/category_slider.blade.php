<style type="text/css">
   .owl-stage-outer
   {
   position: relative;
   }
   .owl-nav
   {
   position: absolute;top: -5px;width: 100%;display: block;
   }
   .owl-prev
   {
   float: left;
   position: absolute;  
   left: -55px;   
   }
   button:focus {outline:0;}
   .owl-next {
   position: absolute;   
   right: -35px;
   }
   .owl-prev:hover
   {
   background:red
   margin-top: 200px;
   }
   .owl-prev i, .owl-next i {transform : scale(1,1.3); color: white;background-color: #DBDBDB;padding: 10px 17px 22px 17px;border-radius: 50%;height: 14px;
   }
   .owl-dot
   {
   visibility: hidden;
   }
   .totot{
   background-color: #F6F6F6 !important;height: 206px;margin-top: 89px;padding-top: 27px; 
   }
   @media only screen and (max-width: 768px) {
   .totot
   {
   margin-top: 70px;padding-bottom: 20px;
   }
   }
</style>
<div class="totot">
   <div class="container-fluid first_div shadow-sm" style="background-color:white;padding: 25px 55px 0px 75px;border:.5px solid #F1F1F1;border-radius: 0px;width: 89%;">
      <div class="row">
         <div class="owl-carousel owl-theme" id="owl-one"  style="transform: translate3d(0px, 7px, 0px);">
            <?php foreach($category as $category) {?>
            <div class="item" >
                <a href="{{ route('product',$category->cat_id) }}">
               <center>
                  <img src="{{ url($category->image ) }} " class="sli_imag">
                  <p class="sli_p"><?php echo $category->title;?></p>
               </center>
            </a>
            </div>
            <?php }?>
           
         </div>
      </div>
   </div>
</div>
<script>
   $(document).ready(function(){
    
       $('.ndiv99').hover(function(){     
           $('.mdiv00').toggle();    
         
       });
     
   });
</script>