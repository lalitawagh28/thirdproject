<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta content="width=device-width, initial-scale=1.0" name="viewport">
      <title>Home Page</title>
      <meta content="" name="descriptison">
      <meta content="" name="keywords">
      <meta name="csrf-token" content="{{ csrf_token() }}" />
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Dosis:300,400,500,,600,700,700i|Lato:300,300i,400,400i,700,700i" rel="stylesheet">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
      <link href="{{ url('frontassets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/venobox/venobox.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/line-awesome/css/line-awesome.min.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <link href="{{ url('frontassets/css/style.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/css/style2.css') }}" rel="stylesheet">
   </head>
   <body>
       @include("web.header")
      <style type="text/css">
       
         @media only screen and (max-width: 768px) {
         .checkout_cardbody
         {
         width: 100%;margin-left: 0px !important;
         }
         .checkout_input
         {
         max-width:95%;
         }
         .checkout_next
         {
         width: 100%;
         margin-top: 15px;margin-left: 0px;
         }
         .checkout_order
         {
         width: 100%;
         }
         #nono
         {
         min-height: 320px;
         }
         #nono2
         {
         min-height: 80px;
         }
         }
      </style>
      <div class="container-fluid checkout_cont">
         <div class="row">
            <div class="col-xl-8">
               <?php if(session('userid') == ''){ ?>
               <div class="card shadow checkout_card">
                  <div class="card-header checkoutcard_head">
                     <h4 class="checkout_h4"><span class="first_num3" style="background-color: #ECECEC;">1</span> Log In / Register <span style="float: right;position: relative;top: 5px;color: blue">Change</span></h4>
                  </div>
               </div>
               <div class="card shadow checkout_card">
                  <div class="card-header checkoutcard_head2">
                     <h4 style="color: white;font-weight: 500;font-size: 17px;"><span class="first_num3">1</span> Log In / Register</h4>
                  </div>
                  <div class="card-body checkout_cardbody" id="nono">
                     <h6 style="font-size: 17px;margin-top: 20px;">As A Guest User <span style="float: right;color: #3571B7;">Already Have An Account?</span></h6>
                     <p class="card-text" style="margin-top: 30px;color: #717171;font-weight: 500;font-size: 15px;">We need to verify your phone number so that we can update you about your order.</p>
                     <p style="font-size: 14px;font-weight: 500;">Enter Your 10 Digit Mobile Number</p>
                     <div class="input-group" style="height: 45px;margin-bottom: 20px;margin-top: -9px;">
                        <div class="input-group-prepend">
                           <span class="input-group-text" id="basic-addon1">+91</span>
                        </div>
                        <input type="text" class="form-control checkout_input"  placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
                        <button class="checkout_next">NEXT</button>
                     </div>
                  </div>
               </div>
               <?php } else {

               $addressdetails = DB::table('address')->where(array('user_id' => session('userid')))->first();


               ?>

               <div class="card shadow checkout_card">
                  <div class="card-header checkoutcard_head">
                     <h4 class="checkout_h4"><span class="first_num3" style="background-color: #ECECEC;">2</span> Delivery Address</h4>
                  </div>
               </div>
               <div class="card shadow checkout_card">
                  <div class="card-header checkoutcard_head2" id="nono2">
                     <h4 style="color: white;font-weight: 500;font-size: 17px;"><span class="first_num3">2</span> Delivery Address <!-- <span style="float: right;position: relative;top: 10px;font-size: 14px;"><i class="fas fa-plus-circle"></i> Add New Address</span> --></h4>
                  </div>
                  <div class="card-body checkout_cardbody">
                     <div class="row" style="margin-top: 20px;">
                        <div class="col-xl-6">
                           <div class="form-group">
                              <label for="usr" style="font-size: 14px;font-weight: 500;">Name <span style="color: red">*</span></label>
                              <input type="text" class="checkout_input22" id="usr" name="deliveryname" value="{{ $addressdetails->receiver_name}}">
                           </div>
                        </div>
                        <div class="col-xl-6">
                           <div class="form-group">
                              <label for="usr" style="font-size: 14px;font-weight: 500;">Mobile <span style="color: red">*</span></label>
                              <input type="text" class="checkout_input22" id="usr" name="deliverymobile" value="{{ $addressdetails->receiver_phone}} ">
                           </div>
                        </div>
                        <div class="col-xl-6">
                           <div class="form-group">
                              <label for="usr" style="font-size: 14px;font-weight: 500;">Flat / House No / Building Name <span style="color: red">*</span></label>
                              <input type="text" class="checkout_input22" id="usr"  name="deliveryhouse" value="{{ $addressdetails->house_no}} ">
                           </div>
                        </div>
                        <div class="col-xl-6">
                           <div class="form-group">
                              <label for="usr" style="font-size: 14px;font-weight: 500;">Area <span style="color: red">*</span></label>
                              <input type="text" class="checkout_input22" id="usr" name="deliveryarea" value="{{ $addressdetails->society}}">
                               
                           </div>
                        </div>
                        <div class="col-xl-6">
                           <div class="form-group">
                              <label for="usr" style="font-size: 14px;font-weight: 500;">Landmark <span style="color: red">*</span></label>
                              <input type="text" class="checkout_input22" id="usr" name="deliverylandmark" value="{{ $addressdetails->landmark}}">
                           </div>
                        </div>
                        <div class="col-xl-6">
                           <div class="form-group">
                              <label for="usr" style="font-size: 14px;font-weight: 500;">Pincode <span style="color: red">*</span></label>
                              <input type="text" class="checkout_input22" id="usr" name="deliverypincode" value="{{ $addressdetails->pincode}}">
                           </div>
                        </div>
                        <div class="col-xl-6">
                           <div class="form-group">
                              <label for="usr" style="font-size: 14px;font-weight: 500;">City <span style="color: red">*</span></label>
                              <input type="text" class="checkout_input22" id="usr"  name="deliverycity" value="{{ $addressdetails->city}}">
                           </div>
                        </div>
                        
                        <div class="col-xl-6">
                           <div class="form-group">
                              <label for="usr" style="font-size: 14px;font-weight: 500;">State <span style="color: red">*</span></label>
                              <input type="text" class="checkout_input22" id="usr"  name="deliverystate" value="{{ $addressdetails->state}}">
                           </div>
                        </div>
                        <div class="col-xl-6">
                           <h5 class="card-title" style="font-size: 15px;margin-top: 45px;"> <input type='radio'  name="gender" checked="checked" />&nbsp;&nbsp;Home &nbsp;&nbsp;&nbsp;<input type='radio'  name="gender" checked="checked" />&nbsp;&nbsp;Office &nbsp;&nbsp;&nbsp;<input type='radio'  name="gender" checked="checked" />&nbsp;&nbsp;Other</h5>
                        </div>
                     </div>
                      
                     <hr style="border-bottom: 2px solid black">
                     <div class="row" style="padding-left: 20px;">
                        
                     <input type="checkbox" name="billingtoo" onclick="FillBilling(this)"  style="display:block;margin-top: 5px;" class="">
                        
                     <span style="padding-left: 20px;">Check this box if Delivery Address and Billing Address are the same.</span>
                    
                     </div>
                     <div class="row" style="margin-top: 20px;">
                        <div class="col-xl-12">
                           <h5 style="margin-top: 20px;margin-bottom: 30px;">BILLING ADDRESS</h5>
                        </div>
                        <div class="col-xl-6">
                           <div class="form-group">
                              <label for="usr" style="font-size: 14px;font-weight: 500;">Full Name <span style="color: red">*</span></label>
                              <input type="text" class="checkout_input22" id="usr" name="billingname" >
                           </div>
                        </div>
                        <div class="col-xl-6">
                           <div class="form-group">
                              <label for="usr" style="font-size: 14px;font-weight: 500;">Mobile <span style="color: red">*</span></label>
                              <input type="text" class="checkout_input22" id="usr" name="billingmobile">
                           </div>
                        </div>
                       <div class="col-xl-6">
                           <div class="form-group">
                              <label for="usr" style="font-size: 14px;font-weight: 500;">Flat / House No / Building Name <span style="color: red">*</span></label>
                              <input type="text" class="checkout_input22" id="usr"  name="billinghouse" >
                           </div>
                        </div>
                        <div class="col-xl-6">
                           <div class="form-group">
                              <label for="usr" style="font-size: 14px;font-weight: 500;">Area <span style="color: red">*</span></label>
                              <input type="text" class="checkout_input22" id="usr" name="billingarea">
                               
                           </div>
                        </div>
                        <div class="col-xl-6">
                           <div class="form-group">
                              <label for="usr" style="font-size: 14px;font-weight: 500;">Landmark <span style="color: red">*</span></label>
                              <input type="text" class="checkout_input22" id="usr" name="billinglandmark" >
                           </div>
                        </div>
                        <div class="col-xl-6">
                           <div class="form-group">
                              <label for="usr" style="font-size: 14px;font-weight: 500;">Pincode <span style="color: red">*</span></label>
                              <input type="text" class="checkout_input22" id="usr" name="billingpincode" >
                           </div>
                        </div>
                        <div class="col-xl-6">
                           <div class="form-group">
                              <label for="usr" style="font-size: 14px;font-weight: 500;">City <span style="color: red">*</span></label>
                              <input type="text" class="checkout_input22" id="usr"  name="billingcity" >
                           </div>
                        </div>
                        
                        <div class="col-xl-6">
                           <div class="form-group">
                              <label for="usr" style="font-size: 14px;font-weight: 500;">State <span style="color: red">*</span></label>
                              <input type="text" class="checkout_input22" id="usr"  name="billingstate" >
                           </div>
                        </div>
                           <div class="col-xl-6">
                           <h5 style="margin-top: 20px;margin-bottom: 30px;">Payement Method</h5>
                              
                                 <div class="form-group">

                                 <label class="form-check-label" style="padding-left: 30px;">
                                 <input type="radio" class="form-check-input" name="types" id="types" value="COD">Cash On Delivery</label>
                                </div>
                                <div class="form-group">

                                 <label class="form-check-label" style="padding-left: 30px;">
                                 <input type="radio" class="form-check-input" name="types" id="types" value="Online">Online Payement</label>
                           </div>
                           </div>
                           <br><br>
                        
                     </div>
                     <div class="col-xl-6">
                           <button class="checkout_continue" id="nextbutton">Continue</button>
                        </div>
                  </div>
               </div>
               <div class="card shadow checkout_card" id="timedate">
                  <div class="card-header checkoutcard_head">
                     <h4 class="checkout_h4"><span class="first_num3" style="background-color: #ECECEC;">3</span> Delivery Date & Time</h4>
                  </div>
               </div>
               <div class="card shadow checkout_card" style="background-color: #F7F7F7">
                  <div class="card-header checkoutcard_head2">
                     <h4 style="color: white;font-weight: 500;font-size: 17px;"><span class="first_num3">3</span> Delivery Date & Time</h4>
                  </div>
                  <div class="card-body checkout_cardbody">
                     <center>
                        <img src="assets/img/dboy.png" style="height: 230px;margin-top: 20px;margin-bottom: 20px;">
                        <h4>Your Order Will be Delivered Within Next 24 Hours At </h4>
                        <hr style="border-bottom: 2px solid black;margin-top: 20px;width: 30%;margin-bottom: 20px;">
                        <p style="color: #A6A6A6;font-size: 15px;">{{$addressdetails->house_no}} {{$addressdetails->society}} {{$addressdetails->landmark}} Pincode-{{$addressdetails->pincode}} {{$addressdetails->city}},{{$addressdetails->state}}</p>
                        <button class="checkout_order"  onclick="placeorder()">Place Order</button>
                     </center>
                  </div>
               </div>
               <div class="modal fade" id="cong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document" style="top: 50px;">
                     <div class="modal-content">
                        <div class="modal-header" style="border-bottom: 0">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                           <span aria-hidden="true">&times;</span>
                           </button>
                        </div>
                        <div class="modal-body" style="padding: 30px 20px 20px 40px;">
                           <center>
                              <!-- <img src="assets/img/cong.PNG" style="height: 150px;margin-top: -50px;margin-bottom: 20px;"> -->
                              <h3 style="color: #4BA345;">Congratulations!</h3>
                              <h3 style="margin-top: -2px;"><b>Your Order Has Been Placed</b></h3>
                              <!-- <p style="font-size: 18px;font-weight: 500;">Order ID :</p>
                              <p style="font-size: 18px;margin-top: -15px;">150340001178606</p> -->
                              <a href="{{ route('index') }}"><button style="background-color: #1461C9;color: white;height: 47px;width: 64%;border-radius: 25px;font-size: 17px;margin-top: 13px;border: 0;font-weight: 500;margin-bottom: 20px;" class="shadow">Continue Shopping</button></a>
                           </center>
                        </div>
                     </div>
                  </div>
               </div>
               <?php }?>
            </div>
            <div class="col-xl-4">
               <div class="card shadow checkout_card">
                  <h6 style="padding:17px 15px 17px 15px"><i class="fas fa-shopping-cart" style="color: #4BA345;"></i> &nbsp;Your Cart <span style="float: right;position: relative;top: 3px;">({{count($cartitem)}} items)</span></h6>
               </div>
               <div class="card shadow checkout_card">

                  <?php $sum=0; foreach($cartitem as $cartitemkey => $cartitem) { 
                        $productdetails = DB::table('product')->where(array('product_id' => $cartitem->product_id))->first();
                        $productvarient = DB::table('product_varient')->where(array('varient_id' => $cartitem->varient_id))->first();
                        $storedata = DB::table('store_products')->where(array('varient_id' => $cartitem->varient_id))->first();
                        ?>
                  <div class="row" style="min-width: 100%;margin-left: 0px;padding-top: 14px;">
                     <div class="col-sm-3" style="max-width: 20%;border-left: 10px solid #41CF2E;"><img src="{{ url($productvarient->varient_image) }}" style="height: 60px;"></div>
                     <div class="col-sm-5" style="max-width: 45%;">
                        <p style="white-space: pre-line;font-size: 14px;margin-bottom: 3px;">{{$productdetails->product_name}}</p>
                        <div class="btn-group btn-group-sm outer_but772" role="group" aria-label="...">
                           Quantity : {{ $cartitem->qty }}
                           <!-- <button class="plus-one inner_one{{$cartitemkey}}" onclick="getminus({{$productvarient->base_price}},{{$cartitem->cart_id}},{{$cartitemkey}})" ><i class="fas fa-minus inner_one_i2" ></i></button>
                              <button class="plus-middle inner_two{{$cartitemkey}}" ><span class="inner_two_span_cart_item{{$cartitemkey}}">{{ $cartitem->qty }}</span></button>
                              <button class="plus-two inner_three{{$cartitemkey}}" onclick="getplus({{$productvarient->base_price}},{{$cartitem->cart_id}},{{$cartitemkey}})"><i class="fas fa-plus inner_three_i2" ></i></button> -->

                        </div>
                     </div>
                     <div class="col-sm-4" style="max-width: 29.5%;">
                        <p id="price{{$cartitemkey}}"  style="font-size: 14px;font-weight: 500;">Rs.{{ $cartitem->qty * $productvarient->base_price }}</p>
                     </div>
                  </div>
                  <hr>
                  <?php $sum += $cartitem->qty * $productvarient->base_price; }  ?>
               </div>
               <input type="hidden" id="subtotaldata" name="subtotaldata" value="{{$sum}}">
               <div class="card shadow checkout_card">
                  <h6 style="padding:17px 15px 17px 15px;width: 88%;">Subtotal: <span id="subtotal" style="float: right;position: relative;top: 3px;">Rs.{{$sum}}</span></h6>
                  <h6 style="padding:17px 15px 17px 15px;width: 88%;">Delivery Charge: <span id="charge" style="float: right;position: relative;top: 3px;"><?php if ($sum > 1000) {
                     $deliverycharges = "Free";
                     echo   $deliverycharges;}
                     else
                     {
                        $deliverycharges =  $delivery ->del_charge;
                        echo   $deliverycharges;
                     }

                      ?></span></h6>
                  <h6 style="padding:17px 15px 17px 15px;margin-top: -24px;width: 88%;">Total: <span id="total" style="float: right;position: relative;top: 3px;">Rs.<?php if($deliverycharges == "Free"){ echo $sum; }else{ echo $sum + $deliverycharges;} ?></span></h6>
                  <input type="hidden"  id="finalamount" name="finalamount" value="<?php if($deliverycharges == "Free"){ echo $sum; }else{ echo $sum + $deliverycharges;} ?>">
                   <input type="hidden"  id="subtotal" name="subtotal" value="<?php echo $sum; ?>">
                   <input type="hidden"  id="addressid" name="addressid" value="<?php echo $addressdetails->address_id; ?>">
                   <input type="hidden" name="storeid" id="storeid" value="<?php echo $storedata->store_id;?>">
                    <input type="hidden" name="deliverycharges" id="deliverycharges" value="<?php echo $deliverycharges;;?>">
               </div>
            </div>
         </div>
      </div>
      <br><br>
       @include("web.footer")
      <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
      <!-- Vendor JS Files -->
      <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
      <!-- Vendor JS Files -->
      <script src="{{ url('frontassets/vendor/jquery/jquery.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/php-email-form/validate.js') }}"></script>
      <script src="{{ url('frontassets/vendor/venobox/venobox.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/counterup/counterup.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
      <!-- Template Main JS File -->
      <script src="{{ url('frontassets/js/main.js') }}"></script>
      <script type="text/javascript">
         function FillBilling(the) {
            
           var checkBox = document.getElementById(the);
            if($(the).prop('checked')) {
           
             $('input[name=billingname]').val($('input[name=deliveryname]').val());
             $('input[name=billingmobile]').val($('input[name=deliverymobile]').val());
             $('input[name=billinghouse]').val($('input[name=deliveryhouse]').val());
             $('input[name=billingarea]').val($('input[name=deliveryarea]').val());
             $('input[name=billinglandmark]').val($('input[name=deliverylandmark]').val());
             $('input[name=billingcity]').val($('input[name=deliverycity]').val());
             $('input[name=billingpincode]').val($('input[name=deliverypincode]').val());
             $('input[name=billingstate]').val($('input[name=deliverystate]').val());
             
             // f.billingcity.value = f.deliverycity.value;
             // f.billingcity.value = f.deliverycity.value;
             // f.billingcity.value = f.deliverycity.value;

           }
         }

         $("#nextbutton").click(function() {
            $('html,body').animate({
               scrollTop: $("#timedate").offset().top},
        'slow');
            });
   function getminus(baseprice,cartid,count)
   {
      var data = $('.inner_two_span_cart_item'+count).html();
      
      if(data > 0)
      {
         var minusdata = parseInt(data) - 1;
          $.ajaxSetup({
                     headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     }
            });
            $.ajax({
               url: "{{ route('updateqtycart') }}",
               type:"POST",
               data:{cartid: cartid,data:minusdata},
               success: function(result){
                if(result == 1)
                {
                    $('.inner_two_span_cart_item'+count).html(minusdata);
                    var total = parseFloat(baseprice) * parseFloat(minusdata);
                    $('#price'+count).html('Rs. '+total);
                    var subtotaldata = $('#delcharge').val();
                    alert(subtotaldata);
                    $('#subtotal').html('Rs. '+total);
                    $('#total').html('Rs. '+subtotaldata);

                }else 
                {

                }
                }
            });
        
      }else 
      {
         $('.inner_two_span_cart_item'+count).html(0);
      }
      
     
   }
   function getplus(baseprice,cartid,count)
   {
      var data = $('.inner_two_span_cart_item'+count).html();
      
      var plusdata = parseInt(data) + 1;


          $.ajaxSetup({
                     headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     }
            });
            $.ajax({
               url: "{{ route('updateqtycart') }}",
               type:"POST",
               data:{cartid: cartid,data:plusdata},
               success: function(result){
                if(result == 1)
                {
                    $('.inner_two_span_cart_item'+count).html(plusdata);
                     var total = parseFloat(baseprice) * parseFloat(plusdata);
                    $('#price'+count).html('Rs. '+total);
                    var subtotaldata = $('#delcharge').val();
                    $('#subtotal').html('Rs. '+total);
                    $('#total').html('Rs. '+subtotaldata);

                }else 
                {

                }
                }
            });
      
   }
   function placeorder()
   {
         var addressid     = $('#addressid').val();
         var subtotal      = $('#subtotaldata').val();
         var storeid       = $('#storeid').val();
         var finalamount   = $('#finalamount').val();
         var paymentmethod = $("input[name='types']:checked").val();
         var deliverycharges = $("#deliverycharges").val();
          $.ajaxSetup({
                     headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     }
            });
          $.ajax({
               url: "{{ route('placeorder') }}",
               type:"POST",
               data:{addressid: addressid,subtotal:subtotal,storeid:storeid,finalamount:finalamount,paymentmethod:paymentmethod,deliverycharges:deliverycharges},
               success: function(result){
                  if(result == 1)
                  {
                        $("#cong"). modal('show');
                  }
               }
         });
   }
   </script>
   
   </body>
</html>