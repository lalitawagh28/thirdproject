<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta content="width=device-width, initial-scale=1.0" name="viewport">
      <title>Store Page</title>
      <meta content="" name="descriptison">
      <meta content="" name="keywords">
      <meta name="csrf-token" content="{{ csrf_token() }}" />
      <!-- Google Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Dosis:300,400,500,,600,700,700i|Lato:300,300i,400,400i,700,700i" rel="stylesheet">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
      <!-- Vendor CSS Files -->
      <link href="{{asset('frontassets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
      <link href="{{asset('frontassets/vendor/icofont/icofont.min.css')}}" rel="stylesheet">
      <link href="{{asset('frontassets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
      <link href="{{asset('frontassets/vendor/venobox/venobox.css')}}" rel="stylesheet">
      <link href="{{asset('frontassets/vendor/line-awesome/css/line-awesome.min.css')}}" rel="stylesheet">
      <link href="{{asset('frontassets/vendor/owl.carousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <link href="{{asset('frontassets/css/style.css')}}" rel="stylesheet">
      <link href="{{asset('frontassets/css/style2.css')}}" rel="stylesheet">
   </head>
   <style type="text/css">
                  .store_font
                  {
                  font-size: 14px;margin-top: 5px;  
                  }
                  .store_font2
                  {
                  font-size: 14px;margin-top: -7px;  
                  }
               </style>
   <body>
     
      @include("web.header")
      @include("web.category_slider")
      <!-- ======= Services Section ======= -->
      <section id="services" class="services section-bg">
         <div class="container-fluid" style="width: 92.2%;">
            <div class="section-title">
               <h2>Recommended Shops</h2>
            </div>
            <div class="row">
               
               <div class="col-lg-4 col-md-6">
                 
                    <div class="icon-box" >
                     <a href="home.php" style="color: black">
                     <div class="icon"><i class="fas fa-store-alt" style="color:#41cf2e;"></i></div>
                     <h4 class="title" style="font-size: 18px;">Raman</h4>
                     <p class="description store_font2">RAMAN STORE SHOP NO. 14. 15 KANCHANJUNGA MARKET NOIDA,Noida,Uttar Pradesh-201301</p>
                     <p class="description store_font">Open Now : 12:01 am till 11:59 pm</p>
                     <p class="description store_font">Distance : 0 km</p>
                     <p class="description store_font">Items Available : 515</p>
                     <p class="description store_font"><b> 
                        <span style="color: #41CF2E;">HOME DELIVERY AVAILABLE</span> <br>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        </b>
                     </p>
                     <p class="description store_font">Pay Online </p>
                      </a>
                  </div>
                 
               </div>


               <div class="col-lg-4 col-md-6">
                 
                    <div class="icon-box" >
                     <a href="home.php" style="color: black">
                     <div class="icon"><i class="fas fa-store-alt" style="color:#41cf2e;"></i></div>
                     <h4 class="title" style="font-size: 18px;">Raman</h4>
                     <p class="description store_font2">RAMAN STORE SHOP NO. 14. 15 KANCHANJUNGA MARKET NOIDA,Noida,Uttar Pradesh-201301</p>
                     <p class="description store_font">Open Now : 12:01 am till 11:59 pm</p>
                     <p class="description store_font">Distance : 0 km</p>
                     <p class="description store_font">Items Available : 515</p>
                     <p class="description store_font"><b> 
                        <span style="color: #41CF2E;">HOME DELIVERY AVAILABLE</span> <br>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        </b>
                     </p>
                     <p class="description store_font">Pay Online </p>
                      </a>
                  </div>
                 
               </div>


               <div class="col-lg-4 col-md-6">
                 
                    <div class="icon-box" >
                     <a href="home.php" style="color: black">
                     <div class="icon"><i class="fas fa-store-alt" style="color:#41cf2e;"></i></div>
                     <h4 class="title" style="font-size: 18px;">Raman</h4>
                     <p class="description store_font2">RAMAN STORE SHOP NO. 14. 15 KANCHANJUNGA MARKET NOIDA,Noida,Uttar Pradesh-201301</p>
                     <p class="description store_font">Open Now : 12:01 am till 11:59 pm</p>
                     <p class="description store_font">Distance : 0 km</p>
                     <p class="description store_font">Items Available : 515</p>
                     <p class="description store_font"><b> 
                        <span style="color: #41CF2E;">HOME DELIVERY AVAILABLE</span> <br>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        </b>
                     </p>
                     <p class="description store_font">Pay Online </p>
                      </a>
                  </div>
                 
               </div>


               <div class="col-lg-4 col-md-6">
                 
                    <div class="icon-box" >
                     <a href="home.php" style="color: black">
                     <div class="icon"><i class="fas fa-store-alt" style="color:#41cf2e;"></i></div>
                     <h4 class="title" style="font-size: 18px;">Raman</h4>
                     <p class="description store_font2">RAMAN STORE SHOP NO. 14. 15 KANCHANJUNGA MARKET NOIDA,Noida,Uttar Pradesh-201301</p>
                     <p class="description store_font">Open Now : 12:01 am till 11:59 pm</p>
                     <p class="description store_font">Distance : 0 km</p>
                     <p class="description store_font">Items Available : 515</p>
                     <p class="description store_font"><b> 
                        <span style="color: #41CF2E;">HOME DELIVERY AVAILABLE</span> <br>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        </b>
                     </p>
                     <p class="description store_font">Pay Online </p>
                      </a>
                  </div>
                 
               </div>


               <div class="col-lg-4 col-md-6">
                 
                    <div class="icon-box" >
                     <a href="home.php" style="color: black">
                     <div class="icon"><i class="fas fa-store-alt" style="color:#41cf2e;"></i></div>
                     <h4 class="title" style="font-size: 18px;">Raman</h4>
                     <p class="description store_font2">RAMAN STORE SHOP NO. 14. 15 KANCHANJUNGA MARKET NOIDA,Noida,Uttar Pradesh-201301</p>
                     <p class="description store_font">Open Now : 12:01 am till 11:59 pm</p>
                     <p class="description store_font">Distance : 0 km</p>
                     <p class="description store_font">Items Available : 515</p>
                     <p class="description store_font"><b> 
                        <span style="color: #41CF2E;">HOME DELIVERY AVAILABLE</span> <br>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        </b>
                     </p>
                     <p class="description store_font">Pay Online </p>
                      </a>
                  </div>
                 
               </div>


               <div class="col-lg-4 col-md-6">
                 
                    <div class="icon-box" >
                     <a href="home.php" style="color: black">
                     <div class="icon"><i class="fas fa-store-alt" style="color:#41cf2e;"></i></div>
                     <h4 class="title" style="font-size: 18px;">Raman</h4>
                     <p class="description store_font2">RAMAN STORE SHOP NO. 14. 15 KANCHANJUNGA MARKET NOIDA,Noida,Uttar Pradesh-201301</p>
                     <p class="description store_font">Open Now : 12:01 am till 11:59 pm</p>
                     <p class="description store_font">Distance : 0 km</p>
                     <p class="description store_font">Items Available : 515</p>
                     <p class="description store_font"><b> 
                        <span style="color: #41CF2E;">HOME DELIVERY AVAILABLE</span> <br>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        </b>
                     </p>
                     <p class="description store_font">Pay Online </p>
                      </a>
                  </div>
                 
               </div>


               <div class="col-lg-4 col-md-6">
                 
                    <div class="icon-box" >
                     <a href="home.php" style="color: black">
                     <div class="icon"><i class="fas fa-store-alt" style="color:#41cf2e;"></i></div>
                     <h4 class="title" style="font-size: 18px;">Raman</h4>
                     <p class="description store_font2">RAMAN STORE SHOP NO. 14. 15 KANCHANJUNGA MARKET NOIDA,Noida,Uttar Pradesh-201301</p>
                     <p class="description store_font">Open Now : 12:01 am till 11:59 pm</p>
                     <p class="description store_font">Distance : 0 km</p>
                     <p class="description store_font">Items Available : 515</p>
                     <p class="description store_font"><b> 
                        <span style="color: #41CF2E;">HOME DELIVERY AVAILABLE</span> <br>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        <i class="far fa-star"></i>
                        </b>
                     </p>
                     <p class="description store_font">Pay Online </p>
                      </a>
                  </div>
                 
               </div>
               






            </div>
         </div>
      </section>
      <!-- End Services Section -->
      <main id="main">
         <!-- ======= About Section ======= -->
         <section id="about" class="about">
            <div class="container" style="margin-top: -50px;margin-bottom: -30px;">
               <div class="row">
                  <div class="col-xl-4 col-lg-4 d-flex justify-content-center align-items-stretch">
                     <div class="icon-box">
                        <div class="icon"><i class="fas fa-hand-holding-usd"></i></div>
                        <h4 class="title"><a href="">Best Price & Offers</a></h4>
                        <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 d-flex justify-content-center align-items-stretch">
                     <div class="icon-box">
                        <div class="icon"><i class="fas fa-inbox"></i></div>
                        <h4 class="title"><a href="">Wide Assorment</a></h4>
                        <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 d-flex justify-content-center align-items-stretch">
                     <div class="icon-box">
                        <div class="icon"><i class="fas fa-rupee-sign"></i></div>
                        <h4 class="title"><a href="">Easy Return</a></h4>
                        <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- End About Section -->
      </main>
      <!-- End #main -->
      @include("web.footer")
      <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
      <!-- Vendor JS Files -->
      <script src="frontassets/vendor/jquery/jquery.min.js"></script>
      <script src="frontassets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <script src="frontassets/vendor/jquery.easing/jquery.easing.min.js"></script>
      <script src="frontassets/vendor/php-email-form/validate.js"></script>
      <script src="frontassets/vendor/venobox/venobox.min.js"></script>
      <script src="frontassets/vendor/waypoints/jquery.waypoints.min.js"></script>
      <script src="frontassets/vendor/counterup/counterup.min.js"></script>
      <script src="frontassets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
      <script src="frontassets/vendor/owl.carousel/owl.carousel.min.js"></script>
      <!-- Template Main JS File -->
      <script src="frontassets/js/main.js"></script>
   </body>
</html>
<script>
   $(document).ready(function(){
     $('#owl-one').owlCarousel({
       loop:true,
       margin:10,
       nav:true,
                       
   responsive: {
           0:{
               items:1
           },
           600:{
               items:4
           },
           1000:{
               items:7
           }
       }
   })
      $( ".owl-prev").html('<img src="frontassets/img/l1.png" height="45" style="margin-left:10px;margin-top:30px;" height="55"  class="imgkl2 shadow">');
      $( ".owl-next").html('<img src="frontassets/img/r2.png" height="45" style="margin-right:10px;margin-top:30px;" height="55" class="imgkl2 shadow">');
   });
   
   
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous"></script>
<script type="text/javascript">
   
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
   
    $("#submit-btn").click(function(e){
  
        e.preventDefault();
   
         var name = $("input[name=username]").val();
         var password = $("input[name=password]").val();
         var email = $("input[name=email]").val();
         var mobile = $("input[name=mobile]").val();
        $.ajax({
           type:'POST',
           url:"{{ route('register') }}",
           data:{name:name, password:password, email:email,mobile:mobile},
           success:function(data){
               if(data.success)
               {
                  $('#msgdetails').html('<div class="alert alert-success">'+data.success+'</div>');
               }else
               {
                  $('#msgdetails').html('<div class="alert alert-danger">'+data.error+'</div>');
               }
              
           }
        });
  
   });
   $("#login-btn").click(function(e){
  
        e.preventDefault();
   
         var mobile = $("#mobile").val();
         var password = $("#password").val();
      
        $.ajax({
           type:'POST',
           url:"{{ route('login') }}",
           data:{mobile:mobile, password:password},
           success:function(data){
            if(data.success)
            {
              $('#login').modal('hide');
              swal({
                title: "SUCCESS",
                text: data.success,
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
               
               }).then(function(){
                                      window.location.reload();
                                  })

            }else
            {
                $('#loginmsgdetails').html('<div class="alert alert-danger">'+data.error+'</div>');
            }

            }
  
   });
  });
</script>
<style type="text/css">
   .imgkl2{
   background-color: white;
   }
   .imgkl2:hover
   {
   background: white !important;
   }
</style>