<style type="text/css">
   @media only screen and (max-width: 768px) {
.nav-menu
{
   z-index: 9 !important;
}

   }
</style>
<header id="header" class="fixed-top header_back">
   <div class="container d-flex align-items-center">
      <a href="index.php" class="logo mr-auto"><img src="{{ url('frontassets/img/logo3.PNG')}}" alt="" class="img-fluid header_a"></a>
      <nav class="nav-menu d-none d-lg-block">
         <ul >
<style>
.dropbtn2 {
 border: 0;font-size: 15px;padding-left: 25px;padding-right: 25px;letter-spacing: .6px;background-color: #417DC6;color: white;font-weight: 500;height:40px;
}

.dropdown2 {
  position: relative;
  display: inline-block;
}

.dropdown-content2 {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 201px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content2 a {
  color: black;
  padding: 8px 26px;
  text-decoration: none;
  display: block;
  border-bottom: 1px solid #DDDDDD;
}

.dropdown-content2 a:hover {background-color: #ddd;}

.dropdown2:hover .dropdown-content2 {display: block;}
.dropdown-content2:hover .dropdown-contentnew {display: block;}
.dropdown-contentnew:hover .dropdown-content2 {background-color: #f1f1f1;}
.dropdown-contentnew
{
  position: absolute; left: 200px; top: 0; display: none;
   color: black; background-color: #f1f1f1;
   min-width: 201px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  
}

</style>
<?php 
 
$where= array('user_id'=> session('userid'));
$cartdata = getTableWhere('cart',$where);


 
?>
          
            <li>
               <div class="input-group" style="margin-top: -7px;">
                  <div class="dropdown2">
                  <button class="dropbtn2">Shop By Category &nbsp;<i class="fas fa-angle-down" style="position: relative;top: 1px;"></i></button>
                    <ul class="dropdown-content2">
    <?php $countdata = count($category);foreach($category as $key => $category){?>
    <li  id="maincat<?php echo $key;?>"><a href="#"><span onclick="getsub('<?php echo $category->cat_id;?>','<?php echo $key;?>',<?php echo $countdata;?>)"><?php echo $category->title;?></span>
      <ul class="dropdown-contentnew" id="subcat<?php echo $key;?>"></ul>
      </a></li>
    
    <?php }?></ul>
                  </div>
                  <input type="text" class="form-control store_search" placeholder="search for products...." aria-label="Input group example" aria-describedby="btnGroupAddon">
                  <div class="input-group-prepend">
                     <div class="input-group-text header_search" id="btnGroupAddon"><i class="fas fa-search"></i></div>
                  </div>
               </div>
            </li>
            <li></li>
            @if(session('userid') == '')
            <li><a href="#" style="display: inline-block;" data-toggle="modal" data-target="#login"><i class="far fa-user-circle" style="padding-right: 10px;"></i> login</a> &nbsp; | &nbsp; <a href="#" data-toggle="modal" data-target="#signup" style="display: inline-block;"> Signup</a> </li>
            @else
            <li class="drop-down"><a href="#" ><i class="far fa-user-circle" style="padding-right: 10px;"></i>{{ session('name') }}</a>
               <ul class="haeder_ul">
                 <li> <div class="row cart_headrow"><div class="col-md-3 " ><i class="fa fa-user"></i></div><div class="col-md-3"><a href="{{route('profile')}}">My Profile</a></div></div><hr></li>

                 <!-- <li><div class="row cart_headrow"><div class="col-md-3"><i class="fa fa-list"></i></div><div class="col-md-3"><a href="#">My Orders</a></div></div><hr></li>
                 <li><div class="row cart_headrow"><div class="col-md-3"><i class="fa fa-gift"></i></div><div class="col-md-3"><a href="#">Rewards </a></div></div><hr></li>
                 <li><div class="row cart_headrow"><div class="col-md-3"><i class="fa fa-wallet"></i></div><div class="col-md-3"><a href="#">Wallet</a></div></div><hr></li> -->
                 <li><div class="row cart_headrow"><div class="col-md-3"><i class="fa fa-lock"></i></div><div class="col-md-3"><a href="{{ route('logoutterm') }}">logout</a></div></div><hr></li>
               </ul>
            </li>
            @endif
            <?php if(session('userid') == '') {?>
                <li class="drop-down">
                  <a href="#" onclick="getalert()"> <i class="fas fa-shopping-cart header_li99"></i></a>
                </li>
            <?php } else { ?>
            <li class="drop-down">
                <a href="#"> <i class="fas fa-shopping-cart header_li99"></i></a>
                <ul class="haeder_ul">
                  <li class="header_licart">
                     <p style="margin-top: -10px;"><b>Your Cart</b><span style="float: right;"><b>({{ count($cartdata) }} product)</b></span></p>
                  </li>
                  <li>
                    <?php foreach($cartdata as $cartkey => $cartdata) {
                      $productdetails = DB::table('product_varient')->where(array('product_id' => $cartdata->product_id,'varient_id' => $cartdata->varient_id))->first();
                      $product = DB::table('product')->where(array('product_id' => $productdetails->product_id))->first();
                      ?>
                     <div class="row cart_headrow">
                        <div class="col-sm-3 cart_headimg"><img src="{{ url($productdetails->varient_image) }}" style="height: 60px;"></div>
                        <div class="col-sm-5" style="max-width: 45%;">
                           <p class="cart_headpp">{{ $product->product_name }}</p>
                           <div class="btn-group btn-group-sm outer_but772" role="group" aria-label="...">
                              <button class="plus-one inner_oneuniquedata{{ $cartkey }}"  onclick="getminusdata({{ $cartdata->cart_id }},{{ $cartkey }})" style="height: 22px;"><i class="fas fa-minus inner_one_i2" style=" top: -5px;
                                 position: relative;"></i></button>
                              <button class="plus-middle inner_twouniquedata{{ $cartkey }}" style="height: 22px;"><span class="inner_two_spanuniquedata{{ $cartkey }}" style=" top: -4px;
                                 position: relative;">{{ $cartdata->qty }}</span></button>
                              <button class="plus-two inner_three2{{ $cartkey }}" onclick="getplusdata('<?php echo $cartdata->cart_id?>','<?php echo $cartkey;?>')" style="height: 22px;"><i class="fas fa-plus inner_three_i2" style=" top: -5px;
                                 position: relative;"></i></button>
                           </div>
                        </div>
                        <div class="col-sm-4" style="max-width: 29.5%;">
                           <p style="font-size: 14px;font-weight: 500;">Rs.{{ $productdetails->base_price }} &nbsp;<i class="far fa-times-circle" onclick="removecartitem({{ $cartdata->cart_id}})" style="float: right;position: relative;top: 3.5px;"></i></p>
                        </div>
                     </div>
                     <hr>
                    <?php }?>
                  </li>
                
                    <li><a href="{{ route('cart') }}" style="display: inline-block;">
                       <button class="go_tocart1">Go To Cart</button>  
                    </a>

                    <a href="{{ route('checkout') }}" style="display: inline-block;margin-left: -35px;">
                       <button class="go_tocart1">Checkout</button>  
                    </a>
                    </li>

                    <style type="text/css">
                       .go_tocart1
                       {
                    background-color: #417DC6;color: white;height: 30px;border-radius: 30px;border: 0;padding-left: 15px;padding-right: 15px;
                       }
                    </style>
                </ul>
            </li>
            <?php }?>
         </ul>
      </nav>
      <!-- .nav-menu -->
   </div>
</header>
<!-- End Header -->




<div class="modal fade" id="signup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 99999;">
   <div class="modal-dialog" role="document" style="top: 50px;">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title mix_name" id="exampleModalLabel"><span data-toggle="modal" data-target="#login" data-dismiss="modal" style="cursor: pointer;">Login</span> &nbsp;|&nbsp; <span style="color: #1461C9;">Signup</span></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>

         <div class="modal-body" style="padding: 30px 20px 20px 40px">
         	<div id="msgdetails"></div>
         	<form method="POST" action="{{ url('/register') }}">
         	@csrf
            <div class="input-group" style="margin-top: -7px;">
               <div class="input-group-prepend">
                  <div class="input-group-text modal_div22" id="btnGroupAddon"><i class="far fa-user-circle"></i></div>
               </div>
               <input type="text" id="username2" class="modal_input2" placeholder="Enter Name..." aria-label="Input group example" aria-describedby="btnGroupAddon" name="username">
            </div>
            <div class="input-group" style="margin-top: 17px;">
               <div class="input-group-prepend">
                  <div class="input-group-text modal_div22" id="btnGroupAddon"><i class="fas fa-mobile-alt" style="padding-left: 2.5px;"></i></div>
               </div>
               <input type="text" id="username2" class="modal_input2" placeholder="Enter Mobile Number..." aria-label="Input group example" aria-describedby="btnGroupAddon" name="mobile">
            </div>
            <div class="input-group" style="margin-top: 17px;">
               <div class="input-group-prepend">
                  <div class="input-group-text modal_div22" id="btnGroupAddon"><i class="far fa-envelope"></i></div>
               </div>
               <input type="text" id="username2" class="modal_input2" placeholder="Enter Email Address..." aria-label="Input group example" aria-describedby="btnGroupAddon" name="email">
            </div>
            <div class="input-group " style="margin-top: 17px;">
               <div class="input-group-prepend ">
                  <div class="input-group-text modal_div22" id="btnGroupAddon"><i class="fas fa-key"></i></div>
               </div>
               <input type="text" id="username2" class="modal_input2" placeholder="Enter Password..." aria-label="Input group example" aria-describedby="btnGroupAddon" name="password">
            </div>
            <div class="input-group" style="margin-top: 17px;">
               <div class="input-group-prepend">
                  <div class="input-group-text modal_div22" id="btnGroupAddon"><i class="fas fa-key"></i></div>
               </div>
               <input type="text" id="username2" class="modal_input2" placeholder="Enter Confirm Password..." aria-label="Input group example" aria-describedby="btnGroupAddon" name="confirm_password">
            </div>
            <button class="shadow haeder_button" type="button" id="submit-btn">Sign Up</button>
            <p class="by_sign_up">By signing up, you agree to our Terms And Conditions</p>
        	</form>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 99999;">
   <div class="modal-dialog" role="document" style="top: 50px;">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title mix_name" id="exampleModalLabel"><span style="color: #1461C9;" >Login</span> &nbsp;|&nbsp; <span data-toggle="modal" style="cursor: pointer;" data-target="#signup" data-dismiss="modal">Signup</span></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body" style="padding: 30px 20px 20px 40px">
         	<div id="loginmsgdetails"></div>
         	<form method="POST">
            <div class="input-group" style="margin-top: -7px;">
               <div class="input-group-prepend">
                  <div class="input-group-text modal_div22" id="btnGroupAddon"><i class="fas fa-mobile-alt" style="padding-left: 2.5px;"></i></div>
               </div>
               <input type="text" id="mobile" class="modal_input2" placeholder="Enter Mobile Number..." aria-label="Input group example" aria-describedby="btnGroupAddon" name="username">
            </div>
            <div class="input-group " style="margin-top: 17px;">
               <div class="input-group-prepend ">
                  <div class="input-group-text modal_div22" id="btnGroupAddon"><i class="fas fa-key"></i></div>
               </div>
               <input type="text" id="password" class="modal_input2" placeholder="Enter Password..." aria-label="Input group example" aria-describedby="btnGroupAddon" name="password">
            </div>
            <button class="shadow haeder_button" id="login-btn" type="button">Log In</button>
            <p class="forgot_header" data-toggle="modal" data-target="#forgot_pass" data-dismiss="modal">Forgot Password? <span style="float: right;">Login With OTP</span></p>
        </form>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="forgot_pass" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 99999;">
   <div class="modal-dialog" role="document" style="top: 50px;">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title mix_name" id="exampleModalLabel">Forgot Password</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body" style="padding: 30px 20px 20px 40px">
            <p style="margin-left: 9px;font-weight: 500;font-size: 14px;">Please verify your mobile number</p>
            <div class="input-group " style="margin-top: 17px;">
               <div class="input-group-prepend ">
                  <div class="input-group-text modal_div22" id="btnGroupAddon"><i class="fas fa-key"></i></div>
               </div>
               <input type="text" id="username2" class="modal_input2" placeholder="Enter your 10 digit mobile number" aria-label="Input group example" aria-describedby="btnGroupAddon">
            </div>
            <button style="margin-bottom: 30px;" class="shadow haeder_button">Next</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="location" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 99999;">
   <div class="modal-dialog" role="document" style="top: 50px;">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title mix_name2" id="exampleModalLabel"><span >Select Your Location & Store</span></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body" style="padding: 30px 20px 20px 40px">
            <div class="input-group" style="margin-top: -7px;">
               <div class="input-group-prepend">
                  <div class="input-group-text modal_div22" id="btnGroupAddon"><i class="fas fa-mobile-alt" style="padding-left: 2.5px;"></i></div>
               </div>
               <input type="text" id="username2" class="modal_input2" placeholder="Enter Your Pincode..." aria-label="Input group example" aria-describedby="btnGroupAddon">
            </div>
            <div class="input-group " style="margin-top: 17px;">
               <div class="input-group-prepend ">
                  <div class="input-group-text modal_div22" id="btnGroupAddon"><i class="fas fa-key"></i></div>
               </div>
               <select id="username2" class="modal_input2">
                  <option>Select Area</option>
                  <option>Delhi</option>
                  <option>Noida</option>
               </select>
            </div>
            <p class="or_header">OR </p>
            <a href="index.php"><button style="margin-bottom: 17px;outline: none;" class="shadow haeder_button">Submit</button></a>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
  function getsub(catid,count,maincatcount)
  {
          $.ajaxSetup({
                     headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     }
            });
            $.ajax({
               url: "{{ route('getsubcat') }}",
               type:"POST",
               data:{catid: catid},
               success: function(result){
                  for(var j=0;j <= maincatcount; j++)
                  {
                    $('#subcat'+j).empty();
                    if(count == j)
                    { 
                      $('#subcat'+j).html(result);
                      $('#maincat'+j).css('background-color','skyblue');
                    }else 
                    {
                      // $('#subcat'+j).css('display','none');
                      $('#maincat'+j).css('background-color','#f1f1f1');
                    }
                  }
                  
               }
            });

  }
  function  geturlproduct(cat_id) {
    var url = '{{ route("productdetail", ":slug") }}';
    url = url.replace(':slug', cat_id);
    window.location.href=url;
   
  }
  function getalert()
  {
    alert('Please Login');
  }
  function getminusdata(cartid,count)
  {
      
      var data = $('.inner_two_spanuniquedata'+count).html();
      if(data > 0)
      {
         var minusdata = parseInt(data) - 1;
          $.ajaxSetup({
                     headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     }
            });
            $.ajax({
               url: "{{ route('updateqtycart') }}",
               type:"POST",
               data:{cartid: cartid,data:minusdata},
               success: function(result){
                if(result == 1)
                {
                    $('.inner_two_spanuniquedata'+count).html(minusdata);
                }else 
                {

                }
                }
            });
        
      }else 
      {
         $('.inner_two_spanuniquedata'+count).html(0);
      }
     
   }
   function getplusdata(cartid,count)
   {
      
      var data = $('.inner_two_spanuniquedata'+count).html();
      
      var plusdata = parseInt(data) + 1;


          $.ajaxSetup({
                     headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     }
            });
            $.ajax({
               url: "{{ route('updateqtycart') }}",
               type:"POST",
               data:{cartid: cartid,data:plusdata},
               success: function(result){
                if(result == 1)
                {
                    $('.inner_two_spanuniquedata'+count).html(plusdata);
                }else 
                {

                }
                }
            });
    }
    function removecartitem(cartid)
    {
         $.ajaxSetup({
                     headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     }
            });
            $.ajax({
               url: "{{ route('removecartitem') }}",
               type:"POST",
               data:{cartid: cartid},
               success: function(result){
                    if(result == 1)
                    {
                      alert('Item Remove From Cart Successfully');
                      location.reload();
                    }else
                    {
                      alert('Somthing Went Wrong');
                      location.reload();
                    }
                }
            });
    }
</script>