<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta content="width=device-width, initial-scale=1.0" name="viewport">
      <title>Home Page</title>
      <meta content="" name="descriptison">
      <meta content="" name="keywords">
      <meta name="csrf-token" content="{{ csrf_token() }}" />
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Dosis:300,400,500,,600,700,700i|Lato:300,300i,400,400i,700,700i" rel="stylesheet">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
      <link href="{{ url('frontassets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/venobox/venobox.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/line-awesome/css/line-awesome.min.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <link href="{{ url('frontassets/css/style.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/css/style2.css') }}" rel="stylesheet">
   </head>
   <body>
       @include("web.header")
      @include("web.category_slider")
      <style type="text/css">
         .product_back
         {
         background: linear-gradient(rgba(0, 0, 0, .65), rgba(0, 0, 0, .65)), url('{{ url('assets/img/back.jpeg') }}');
         height: 100px;
         background-size: cover;background-position: 50% 50%;
         }
      </style>
      <div class="product_back">
         <center>
            <h4 style="color: white;padding-top: 33px;">Store Name</h4>
         </center>
      </div>
      <div class="container-fluid" style="width: 92.2%;">
         <img src="{{ url('images/back2.jpg') }}" class="product_mainimg">
      </div>
      <style type="text/css">
         @media only screen and (max-width: 768px) {
         .select_gram
         {
         width: 100%; 
         }
         .product_sidebar
         {
         min-width:85% !important;margin-top: 61px;margin-left: 7px; 
         }
         .product_sidebar2
         {
         min-width:85% !important;margin-top: 20px;margin-left: 7px;
         }
         .product_mainimg
         {
         height: 150px;width: 100%;border-radius: 10px;margin-top: 30px;margin-bottom: 30px; 
         }

         }
         
      </style>
      <div style="background-color: #F4FBFE;">
         <div class="container-fluid" style="width: 92.2%;">
            <div class="row">
               <div class="col-lg-2">
                  <div class="card product_sidebar">
                     <h5 class="side_filter" style="background-color: white;">Category </h5>
                     <div class="card-body side_price_scroll">
                        <form style="margin-left: -10px;">
                           <?php $i= 1; foreach($category as $categorydata){?>
                           <input type="checkbox"  id="fruit<?php echo $i;?>" name="fruit-<?php echo $i;?>" value="<?php echo $categorydata->cat_id;?>" onclick="getproduct('<?php echo $i;?>','<?php echo $categorydata->cat_id?>','<?php echo $cat_id_url;?>')">
                           <label for="fruit<?php echo $i;?>"><?php echo $categorydata->title;?></label>
                           <?php $i++;}?>
                         
                        </form>
                     </div>
                  </div>
                 <!--  <div class="card product_sidebar2">
                     <h5 class="side_filter" style="background-color: white;">Brand  </h5>
                     <div class="card-body side_price_scroll">
                        <form style="margin-left: -10px;">
                           <input type="checkbox" id="fruit1" name="fruit-1" value="Apple">
                           <label for="fruit1">24 Mantra</label>
                           <input type="checkbox" id="fruit2" name="fruit-2" value="afef">
                           <label for="fruit2">American</label>
                           <input type="checkbox" id="fruit3" name="fruit-3" value="dfwefw">
                           <label for="fruit3">Amul</label>
                           <input type="checkbox" id="fruit4" name="fruit-4" value="sdfsd">
                           <label for="fruit4">Amulya</label>
                           <input type="checkbox" id="fruit5" name="fruit-5" value="sdfsd">
                           <label for="fruit5">Apis</label>
                           <input type="checkbox" id="fruit3" name="fruit-3" value="dfwefw">
                           <label for="fruit3">Strawberry</label>
                           <input type="checkbox" id="fruit4" name="fruit-4" value="sdfsd">
                           <label for="fruit4">Strawberry</label>
                           <input type="checkbox" id="fruit5" name="fruit-5" value="sdfsd">
                           <label for="fruit5">Dabur</label>
                           <input type="checkbox" id="fruit5" name="fruit-5" value="sdfsd">
                           <label for="fruit5">Apis</label>
                           <input type="checkbox" id="fruit3" name="fruit-3" value="dfwefw">
                           <label for="fruit3">Strawberry</label>
                           <input type="checkbox" id="fruit4" name="fruit-4" value="sdfsd">
                           <label for="fruit4">Strawberry</label>
                           <input type="checkbox" id="fruit5" name="fruit-5" value="sdfsd">
                           <label for="fruit5">Dabur</label>
                        </form>
                     </div>
                  </div> -->
               </div>
               <style type="text/css">
                  @media only screen and (max-width: 768px) {
.pro_mob29
{
min-width: 164% !important;
}
                  }

                  @media only screen and (max-width: 425px) {
.pro_mob29
{
min-width: 176% !important;
}
                  }


                                    @media only screen and (max-width: 375px) {
.pro_mob29
{
min-width: 152% !important;
}
                  }

                  .pro_mob29
{
min-width: 100%;   
}
               </style>
               <div class="col-lg-10">
                  <!-- ======= Team Section ======= -->
                  <section id="team" class="team section-bg">
                     <div class="container" id="prodctdetailsdiv">
                        <div class="row">
                           <?php $i= 1; foreach($product as $product) {
                              $productvarientwigth = DB::table('product_varient')->where(array('product_id' => $product->product_id))->get();
                              $productvarient = DB::table('product_varient')->where(array('product_id' => $product->product_id))->first();

                             
                              ?>
                           <div class="col-lg-4 col-md-6 " >
                              <!-- <a href="#"> -->
                              <div class="member pro_mob29" stytle="">
                                 <div class="member-img" id="imgdata<?php echo $i;?>">
                                    <center><a href="{{route('productdetail',$product->cat_id)}}"><img src="{{ url($product->product_image) }}" class="img-fluid sel_img23new" alt=""></a></center>
                                 </div>
                                 <div class="member-info">
                                    <h4><?php echo $product->product_name;?></h4>
                                    <select class="select_gram" onchange="getprice(this,'<?php echo $i;?>')">
                                       <?php foreach($productvarientwigth as $productvarientwigth){?>
                                       <option value="<?php echo $productvarientwigth->varient_id;?>" ><?php echo $productvarientwigth->unit;?></option>
                                       <?php }?>
                                    </select>
                                    <input type="hidden" name="varient_id" id="varient_id" value="<?php echo $productvarient->varient_id?>">
                                    <h4 class="h4_rupee" id="productprice<?php echo $i; ?>">Rs. <?php echo $productvarient->base_price;?></h4>
                                    <div class="btn-group btn-group-sm outer_but772" role="group" aria-label="...">
                                       <button class="plus-one inner_one<?php echo $i;?>" onclick="getminus(<?php echo $i;?>)"><i class="fas fa-minus inner_one_i2"></i></button>
                                       <button class="plus-middle inner_two<?php echo $i;?>"><span class="inner_two_span<?php echo $i;?>">1</span></button>
                                       <button class="plus-two inner_three<?php echo $i;?>" onclick="getplus(<?php echo $i;?>)"><i class="fas fa-plus inner_three_i2"></i></button>
                                    </div>
                                     <?php if(session('userid') != '') { ?>
                                    <button class="add_but" onclick="addtobag('<?php echo session('userid') ?>','<?php echo $product->product_id?>','<?php echo $i;?>')">Add to bag</button>
                                    <?php }else {?>
                                    <button class="add_but" onclick="addtobag(0,0,0)">Add to bag</button>
                                    <?php }?>
                                 </div>
                              </div>
                            
                             <!--  </a> -->
                           </div>
                           <?php $i++; }?>
                        </div>
                     </div>
                  </section>
                  <!-- End Team Section -->
               </div>
            </div>
         </div>
      </div>
      <main id="main">
         <!-- ======= About Section ======= -->
         <section id="about" class="about">
            <div class="container" style="margin-top: -50px;margin-bottom: -30px;" >
               <div class="row">
                  <div class="col-xl-4 col-lg-4 d-flex justify-content-center align-items-stretch">
                     <div class="icon-box">
                        <div class="icon"><i class="fas fa-hand-holding-usd"></i></div>
                        <h4 class="title"><a href="">Best Price & Offers</a></h4>
                        <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 d-flex justify-content-center align-items-stretch">
                     <div class="icon-box">
                        <div class="icon"><i class="fas fa-inbox"></i></div>
                        <h4 class="title"><a href="">Wide Assorment</a></h4>
                        <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 d-flex justify-content-center align-items-stretch">
                     <div class="icon-box">
                        <div class="icon"><i class="fas fa-rupee-sign"></i></div>
                        <h4 class="title"><a href="">Easy Return</a></h4>
                        <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- End About Section -->
      </main>
     
      <!-- End #main -->
      @include("web.footer")
      <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
      <!-- Vendor JS Files -->
      <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
      <!-- Vendor JS Files -->
      <script src="{{ url('frontassets/vendor/jquery/jquery.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/php-email-form/validate.js') }}"></script>
      <script src="{{ url('frontassets/vendor/venobox/venobox.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/counterup/counterup.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
      <!-- Template Main JS File -->
      <script src="{{ url('frontassets/js/main.js') }}"></script>
   </body>
</html>
<script>
   $(document).ready(function(){
     $('#owl-one').owlCarousel({
       loop:true,
       margin:10,
       nav:true,
                       
   responsive: {
           0:{
               items:1
           },
           600:{
               items:4
           },
           1000:{
               items:7
           }
       }
   })
      $( ".owl-prev").html('<img src="{{ url("frontassets/img/l1.png") }}" height="45" style="margin-left:10px;margin-top:30px;" height="55"  class="imgkl2 shadow">');
      $( ".owl-next").html('<img src="{{ url("frontassets/img/r2.png") }}" height="45" style="margin-right:10px;margin-top:30px;" height="55" class="imgkl2 shadow">');
   });
   
</script>
<style type="text/css">
   .imgkl2{
   background-color: white;
   }
   .imgkl2:hover
   {
   background: white !important;
   }
</style>
<script type="text/javascript">
   function getprice(the,count)
   {
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
   
      var weight = $(the).val();
      $.ajax({
            url: "{{ route('getpriceweight') }}",
            type:"POST",
            data:{weight: weight},
            success: function(result){
               var obj = JSON.parse(result);
               var urlimg = "{{ url("/") }}/"+obj.productimg;
               $('#productprice'+count).html('Rs.'+obj.productprice);
               $('#imgdata'+count).html(' <center><img src="'+urlimg+'" class="img-fluid sel_img23new" alt=""></center>');
               $('#varient_id').val(weight);
            }
      });
   }
   function getproduct(count,cat_id,catidurl)
   {
      
      if ($('#fruit'+count).is(':checked'))
      {
         
         $.ajaxSetup({
                     headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     }
         });
         $.ajax({
            url: "{{ route('getproductlist') }}",
            type:"POST",
            data:{cat_id: cat_id},
            success: function(result){

               $('#prodctdetailsdiv').html(result);
              
            }
         });
      }
      else
      {
         $.ajaxSetup({
                     headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     }
         });
         $.ajax({
            url: "{{ route('getproductlist') }}",
            type:"POST",
            data:{cat_id: catidurl},
            success: function(result){

               $('#prodctdetailsdiv').html(result);
              
            }
         });
      }

   }
   function getminus(count)
   {
      var data = $('.inner_two_span'+count).html();
      
      
      if(data > 0)
      {
         var minusdata = parseInt(data) - 1;
         $('.inner_two_span'+count).html(minusdata);
      }else 
      {
         $('.inner_two_span'+count).html(0);
      }
     
   }
   function getplus(count)
   {
      var data = $('.inner_two_span'+count).html();
      
      var plusdata = parseInt(data) + 1;
      $('.inner_two_span'+count).html(plusdata);
   }
   function addtobag(userid,pid,count)
   {
         if(userid == 0)
         {
            alert('Please Login !!!');
         }else
         {
            
            var qty = $('.inner_two_span'+count).html();
            var varient = $('#varient_id').val();
            
            $.ajaxSetup({
                     headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     }
            });
            $.ajax({
               url: "{{ route('addtobag') }}",
               type:"POST",
               data:{userid: userid,pid:pid,qty:qty,varient:varient},
               success: function(result){
                  if(result == 1)
                  {
                     alert('Product added to cart successfully');
                  }else if(result == 2){
                     alert('Product allready added to cart ');
                  }else
                  {
                     alert('Product not added to cart');
                  }

               }
            });
         }
         
   }
   
</script>