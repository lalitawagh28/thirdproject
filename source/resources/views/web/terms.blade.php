<!DOCTYPE html>
<html lang="en">

   <head>
      <meta charset="utf-8">
      <meta content="width=device-width, initial-scale=1.0" name="viewport">
      <title>Home Page</title>
      <meta content="" name="descriptison">
      <meta content="" name="keywords">
      <meta name="csrf-token" content="{{ csrf_token() }}" />
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Dosis:300,400,500,,600,700,700i|Lato:300,300i,400,400i,700,700i" rel="stylesheet">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
      <link href="{{ url('frontassets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/icofont/icofont.min.css')}}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/venobox/venobox.css')}}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/line-awesome/css/line-awesome.min.css')}}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/owl.carousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js')}}"></script>
      <link href="{{ url('frontassets/css/style.css')}}" rel="stylesheet">
      <link href="{{ url('frontassets/css/style2.css')}}" rel="stylesheet">
   </head>
   <style type="text/css">
      .owl-stage-outer
      {
      position: relative;
      }
      .owl-nav
      {
      position: absolute;top: -5px;width: 100%;
      }
      .owl-prev
      {
      float: left;
      position: absolute;  
      left: -55px;   
      }
      button:focus {outline:0;}
      .owl-next {
      position: absolute;   
      right: -35px;
      }
      .owl-prev:hover
      {
      background:red
      margin-top: 200px;
      }
      .owl-prev i, .owl-next i {transform : scale(1,1.3); color: white;background-color: #DBDBDB;padding: 10px 17px 22px 17px;border-radius: 50%;height: 14px;
      }
      .owl-dot
      {
      visibility: hidden;
      }
   </style>
   <body>
     @include("web.header")
      @include("web.category_slider")
<style>
.canreq_cont {
    background-color: #f6f2f2;
}
</style>
@section('content')
  <div class="container-fluid canreq_cont" align="center" style="z-index:999999 !important">
         <?= $about->description ?>
     
  </div>
@endsection
@include("web.footer")
      <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
      <!-- Vendor JS Files -->
      <script src="{{ url('frontassets/vendor/jquery/jquery.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/php-email-form/validate.js') }}"></script>
      <script src="{{ url('frontassets/vendor/venobox/venobox.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/counterup/counterup.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
      <!-- Template Main JS File -->
      <script src="{{ url('frontassets/js/main.js') }}"></script>
   </body>
</html>
<script>
   $(document).ready(function(){
     $('#owl-one').owlCarousel({
       loop:true,
       margin:10,
       nav:true,
                       
   responsive: {
           0:{
               items:1
           },
           600:{
               items:4
           },
           1000:{
               items:7
           }
       }
   })
      $( ".owl-prev").html('<img src=" {{ url("frontassets/img/l1.png") }}" height="45" style="margin-left:10px;margin-top:30px;" height="55"  class="imgkl2 shadow">');
      $( ".owl-next").html('<img src="{{ url("frontassets/img/r2.png") }}" height="45" style="margin-right:10px;margin-top:30px;" height="55" class="imgkl2 shadow">');
   });
   
   
</script>  
           