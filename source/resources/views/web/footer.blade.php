<!-- ======= Footer ======= -->
<footer id="footer" >
   <div class="footer-top" style="background-color: #F6F7FC !important;">
      <div class="container">
         <div class="row">
            <div class="col-lg-3 col-md-6 footer-links">
               <h4>Sub Links</h4>
               <ul>
                  <li><i class="bx bx-chevron-right"></i> <a href="{{ route('index') }}">Home</a></li>
                  <li><i class="bx bx-chevron-right"></i> <a href="{{ route('about') }}">About us</a></li>
                  <!-- <li><i class="bx bx-chevron-right"></i> <a href="">Services</a></li> -->
                  <li><i class="bx bx-chevron-right"></i> <a href="{{ route('privacy') }}">Terms of service</a></li>
               </ul>
            </div>
            <div class="col-lg-3 col-md-6 footer-links">
               <h4>For Download</h4>
               <ul>
                  <li><img src="{{ url('frontassets/img/a1.png') }}" style="width: 53%;height: 40px;"></li>
                  <li><img src="{{ url('frontassets/img/g1.png') }}" style="width: 53%;height: 40px;"></li>
                  
               </ul>
            </div>
            <div class="col-lg-3 col-md-6 footer-links">
               <h4>Payment Method</h4>
               <ul>
                  <li><img src="{{ url('frontassets/img/pay1.png')}}" style="width: 63%;position: relative;left: -20px;top: -40px"></li>
                  
               </ul>
            </div>
            <div class="col-lg-3 col-md-6 footer-links">
               <h4>Find Us On</h4>
               <p>Cras fermentum odio eu feugiat lide par naso tierra videa magna derita valies</p>
               <div class="social-links mt-3">
                  
                  <a href="#" class="facebook" style="border-radius: 30px;"><i class="bx bxl-facebook"></i></a>
                  <a href="#" class="instagram" style="border-radius: 30px;"><i class="bx bxl-instagram"></i></a>
                  
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="container py-4">
      <center>
         <div class="copyright" style="margin-bottom: -50px;">
            &copy; Copyright <strong><span>Tecmanic</span></strong>. All Rights Reserved
         </div>
      </center>
   </div>
</footer>
<!-- End Footer -->