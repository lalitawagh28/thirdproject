<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta content="width=device-width, initial-scale=1.0" name="viewport">
      <title>Home Page</title>
      <meta content="" name="descriptison">
      <meta content="" name="keywords">
      <meta name="csrf-token" content="{{ csrf_token() }}" />
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Dosis:300,400,500,,600,700,700i|Lato:300,300i,400,400i,700,700i" rel="stylesheet">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
      <link href="{{ url('frontassets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/venobox/venobox.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/line-awesome/css/line-awesome.min.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <link href="{{ url('frontassets/css/style.css') }}" rel="stylesheet">
      <link href="{{ url('frontassets/css/style2.css') }}" rel="stylesheet">
   </head>
   <style type="text/css">
      
   </style>
   <body>
     @include("web.header")
      @include("web.category_slider")
      <style type="text/css">
      </style>
      <div class="container-fluid" style="width: 91.5%;margin-top: -29px;">
         <div class="row">
            <div class="col-xl-12">
               <div id="demo" class="carousel slide" data-ride="carousel">
                  <!-- Indicators -->
                  <ul class="carousel-indicators home_ul_green" style="top: 500px !important;">
                     <li data-target="#demo" data-slide-to="0" class="active shadow-lg" style=" height: 20px;width: 20px;border:5px solid #41CF2E; "></li>
                     <li data-target="#demo" data-slide-to="1" class="shadow-lg" style=" height: 20px;width: 20px;border:5px solid #41CF2E; "></li>
                     <li data-target="#demo" data-slide-to="2" class="shadow-lg" style=" height: 20px;width: 20px;border:5px solid #41CF2E; "></li>
                  </ul>
                  <!-- The slideshow -->
                  <div class="carousel-inner">
                     <!-- <div class="carousel-item active">
                        <img src="assets/img/h1.jpg" class="home_ban_image" style="height: 520px !important;" alt="Los Angeles">
                     </div> -->
                     <?php $i = 0;foreach($banner as $banner) { 
                        if($i == 0){ ?>
                     <div class="carousel-item active">
                        
                        <img src="<?php echo $banner->banner_image;?>" class="home_ban_image" style="height: 520px !important;" alt="Los Angeles">
                       
                     </div>
                      <?php }else{?>
                       <div class="carousel-item ">
                            <img src="<?php echo $banner->banner_image;?>" class="home_ban_image" style="height: 520px !important;" alt="Los Angeles">
                        </div>
                      <?php } $i++;}?>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
      <br>
      
      
      <div class="container-fluid" style="width: 89.5%;margin-top: -40px;">
         <div class="row" style="margin-top: 30px;margin-bottom: -7px;">
            <div class="col-xl-12">
               <center>
                  <h3 >Top Selling Items</h3>
               </center>
            </div>
         </div>
         <div class="row">
            <div class="owl-carousel owl-theme" id="owl-three"  style="transform: translate3d(0px, 7px, 0px);margin-bottom: 30px;">
               <?php $i=0; foreach($topselling as $key =>  $topselling) {
                  $productvarientwigth = DB::table('product_varient')->where(array('product_id' => $topselling->product_id))->get();
                  $productvarient = DB::table('product_varient')->where(array('product_id' => $topselling->product_id))->first();

                  ?>
               <div class="col-lg-3 col-md-6 d-flex align-items-stretch item shadow home_product">
                  <div class="member">
                     <div class="member-img" id="imgdata<?php echo $i;?>">
                        <center><a href="{{route('productdetail',$topselling->cat_id)}}"><img src="{{ url($topselling->product_image) }}" class="img-fluid sel_img23new" alt=""></a></center>
                     </div>
                     <div class="member-info">
                        <h4 class="head_hfour"><?php echo $topselling->product_name?></h4>
                       <select class="select_gram" onchange="getprice(this,'<?php echo $i;?>')">
                           <?php foreach($productvarientwigth as $productvarientwigth){?>
                           <option value="<?php echo $productvarientwigth->varient_id;?>" ><?php echo $productvarientwigth->quantity.' '.$productvarientwigth->unit;?></option>
                           <?php }?>
                        </select>
                        <input type="hidden" name="varient_id" id="varient_id<?php echo $i;?>" value="<?php echo $productvarient->varient_id?>">
                        <h4 class="h4_rupee" id="productprice<?php echo $i; ?>">Rs. <?php echo $topselling->price?></h4>
                        <div class="btn-group btn-group-sm outer_but772" role="group" aria-label="...">
                           <button class="plus-one inner_one<?php echo $i;?>" onclick="getminus(1,'<?php echo $i;?>')"><i class="fas fa-minus inner_one_i2"></i></button>
                           <button class="plus-middle inner_two<?php echo $i;?>"><span class="inner_two_spanone<?php echo $key;?>">1</span></button>
                           <button class="plus-two inner_three<?php echo $i;?>" onclick="getplus(1,'<?php echo $i;?>')"><i class="fas fa-plus inner_three_i2"></i></button>
                        </div>
                        <?php if(session('userid') != '') { ?>
                        <button class="add_but" onclick="addtobag('1','<?php echo session('userid') ?>','<?php echo $topselling->product_id?>','<?php echo $i;?>')">Add to bag</button>
                        <?php }else {?>
                        <button class="add_but" onclick="addtobag(0,0,0,0)">Add to bag</button>
                        <?php }?>
                     </div>
                  </div>
               </div>
               <?php $i++; }?>
            </div>
         </div>
      </div>


      <div class="container-fluid" style="width: 89.5%;margin-top: -40px;">
         <div class="row" style="margin-top: 30px;margin-bottom: -7px;">
            <div class="col-xl-12">
               <center>
                  <h3 >Newly Added Items</h3>
               </center>
            </div>
         </div>
         <div class="row">
            <div class="owl-carousel owl-theme" id="owl-four"  style="transform: translate3d(0px, 7px, 0px);margin-bottom: 30px;">
               <?php foreach($newlyadded as $newkey =>  $new) {
                   $productvarientwigthtwo = DB::table('product_varient')->where(array('product_id' => $new->product_id))->get();
                  $productvarienttwo = DB::table('product_varient')->where(array('product_id' => $new->product_id))->first();
                  ?>
               <div class="col-lg-3 col-md-6 d-flex align-items-stretch item shadow home_product">
                  <div class="member">
                     <div class="member-img" id="imgdatatwo<?php echo $newkey;?>">
                        <center><a href="{{route('productdetail',$new->cat_id)}}"><img src="{{ url($new->product_image) }}" class="img-fluid sel_img23new" alt=""></a></center>
                     </div>
                     <div class="member-info">
                        <h4 class="head_hfour"><?php echo $new->product_name?></h4>
                      <select class="select_gram" onchange="getpricetwo(this,'<?php echo $newkey;?>')">
                            <?php foreach($productvarientwigthtwo as $productvarientwigthtwo){?>
                           <option value="<?php echo $productvarientwigthtwo->varient_id;?>" ><?php echo $productvarientwigthtwo->quantity.' '.$productvarientwigthtwo->unit;?></option>
                           <?php }?>
                        </select>
                        <h4 class="h4_rupee" id="productpricetwo<?php echo $newkey; ?>">Rs. <?php echo $new->price?></h4>
                         <input type="hidden" name="varient_id" id="varient_idtwo<?php echo $newkey;?>" value="<?php echo $productvarienttwo->varient_id?>">
                        <div class="btn-group btn-group-sm outer_but772" role="group" aria-label="...">
                           <button class="plus-one inner_one<?php echo $newkey;?>" onclick="getminus(2,'<?php echo $newkey;?>')"><i class="fas fa-minus inner_one_i2"></i></button>
                           <button class="plus-middle inner_two<?php echo $newkey;?>"><span class="inner_two_spantwo<?php echo $newkey;?>">1</span></button>
                           <button class="plus-two inner_three<?php echo $newkey;?>" onclick="getplus(2,'<?php echo $newkey;?>')"><i class="fas fa-plus inner_three_i2"></i></button>
                        </div>
                        <?php if(session('userid') != '') { ?>
                        <button class="add_but" onclick="addtobag('2','<?php echo session('userid') ?>','<?php echo $new->product_id?>','<?php echo $newkey;?>')">Add to bag</button>
                        <?php }else {?>
                        <button class="add_but" onclick="addtobag(0,0,0,0)">Add to bag</button>
                        <?php }?>
                     </div>
                  </div>
               </div>
               <?php }?>
            </div>
         </div>
      </div>




      <div class="container-fluid" style="width: 89.5%;margin-top: -40px;">
         <div class="row" style="margin-top: 30px;margin-bottom: -7px;">
            <div class="col-xl-12">
               <center>
                  <h3 >Recently Purchased Items</h3>
               </center>
            </div>
         </div>
         <div class="row">
            <div class="owl-carousel owl-theme" id="owl-five"  style="transform: translate3d(0px, 7px, 0px);margin-bottom: 30px;">
              <?php foreach($recentselling as $thridkey =>  $recentselling) {
               $productvarientwigththree = DB::table('product_varient')->where(array('product_id' => $recentselling->product_id))->get();
                  $productvarientthree = DB::table('product_varient')->where(array('product_id' => $recentselling->product_id))->first();
                  ?>
               <div class="col-lg-3 col-md-6 d-flex align-items-stretch item shadow home_product">
                  <div class="member">
                     <div class="member-img" id="imgdatathree<?php echo $thridkey;?>">
                        <center><a href="{{route('productdetail',$recentselling->cat_id)}}"><img src="{{ url($recentselling->product_image) }}" class="img-fluid sel_img23new" alt=""></a></center>
                     </div>
                     <div class="member-info">
                        <h4 class="head_hfour"><?php echo $recentselling->product_name?></h4>
                         <select class="select_gram" onchange="getpricethree(this,'<?php echo $thridkey;?>')">
                            <?php foreach($productvarientwigththree as  $productvarientwigththree){?>
                           <option value="<?php echo  $productvarientwigththree->varient_id;?>" ><?php echo  $productvarientwigththree->quantity.' '. $productvarientwigththree->unit;?></option>
                           <?php }?>
                        </select>
                        <h4 class="h4_rupee" id="productpricethree<?php echo $thridkey; ?>">Rs. <?php echo $recentselling->price?></h4>
                         <input type="hidden" name="varient_id" id="varient_idthree<?php echo $thridkey;?>" value="<?php echo  $productvarientthree->varient_id?>">
                        <div class="btn-group btn-group-sm outer_but772" role="group" aria-label="...">
                           <button class="plus-one inner_one<?php echo $thridkey;?>" onclick="getminus(3,'<?php echo $thridkey;?>')"><i class="fas fa-minus inner_one_i2"></i></button>
                           <button class="plus-middle inner_two<?php echo $thridkey;?>"><span class="inner_two_spanthree<?php echo $thridkey;?>">1</span></button>
                           <button class="plus-two inner_three<?php echo $thridkey;?>" onclick="getplus(3,'<?php echo $thridkey;?>')"><i class="fas fa-plus inner_three_i2"></i></button>
                        </div>
                        <?php if(session('userid') != '') { ?>
                        <button class="add_but" onclick="addtobag('3','<?php echo session('userid') ?>','<?php echo $recentselling->product_id?>','<?php echo $thridkey;?>')">Add to bag</button>
                        <?php }else {?>
                        <button class="add_but" onclick="addtobag(0,0,0,0)">Add to bag</button>
                        <?php }?>
                     </div>
                  </div>
               </div>
               <?php }?>
            </div>
         </div>
      </div>
      
      <!-- End #main -->
     @include("web.footer")
      <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
      <!-- Vendor JS Files -->
      <script src="{{ url('frontassets/vendor/jquery/jquery.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/php-email-form/validate.js') }}"></script>
      <script src="{{ url('frontassets/vendor/venobox/venobox.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/counterup/counterup.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
      <script src="{{ url('frontassets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
      <!-- Template Main JS File -->
      <script src="{{ url('frontassets/js/main.js') }}"></script>
   </body>
</html>
<script>
   $(document).ready(function(){
     $('#owl-one').owlCarousel({
       loop:true,
       margin:10,
       nav:true,
                       
   responsive: {
           0:{
               items:1
           },
           600:{
               items:4
           },
           1000:{
               items:7
           }
       }
   })
      $( "#owl-one .owl-nav .owl-prev").html('<img src="{{ url("frontassets/img/l1.png") }}" height="45" style="margin-left:10px;margin-top:150px;" height="55"  class="imgkl shadow">');
      $( "#owl-one .owl-nav .owl-next").html('<img src="{{ url("frontassets/img/r2.png") }}" height="45" style="margin-right:-10px;margin-top:150px;" height="55" class="imgkl shadow">');
     
   });
   
   
</script>
<script>
 
   
</script>
<script>
   $(document).ready(function(){
     $('#owl-three').owlCarousel({
       loop:false,
       margin:10,
       nav:true,
                       
   responsive: {
           0:{
               items:1
           },
           600:{
               items:3
           },
           1024:{
               items:5
           },
           1000:{
               items:5
           }
       }
   })
     $( "#owl-three .owl-nav .owl-prev").html('<img src="{{ url("frontassets/img/l1.png") }}" height="45" style="margin-left:10px;margin-top:150px;" height="55"  class="imgkl shadow">');
      $( "#owl-three .owl-nav .owl-next").html('<img src="{{ url("frontassets/img/r2.png") }}" height="45" style="margin-right:-10px;margin-top:150px;" height="55" class="imgkl shadow">');
   });
   
   
</script>
<script>
   $(document).ready(function(){
     $('#owl-four').owlCarousel({
       loop:false,
       margin:10,
       nav:true,
                       
   responsive: {
           0:{
               items:1
           },
           600:{
               items:3
           },
           1024:{
               items:5
           },
           1000:{
               items:5
           }
       }
   })
     $( "#owl-four .owl-nav .owl-prev").html('<img src="{{ url("frontassets/img/l1.png") }}" height="45" style="margin-left:10px;margin-top:150px;" height="55"  class="imgkl shadow">');
      $( "#owl-four .owl-nav .owl-next").html('<img src="{{ url("frontassets/img/r2.png") }}" height="45" style="margin-right:-10px;margin-top:150px;" height="55" class="imgkl shadow">');
   });
   
   
</script>

<script>
   $(document).ready(function(){
     $('#owl-five').owlCarousel({
       loop:false,
       margin:10,
       nav:true,
                       
   responsive: {
           0:{
               items:1
           },
           600:{
               items:3
           },
           1024:{
               items:5
           },
           1000:{
               items:5
           }
       }
   })
     $( "#owl-five .owl-nav .owl-prev").html('<img src="{{ url("frontassets/img/l1.png") }}" height="45" style="margin-left:10px;margin-top:150px;" height="55"  class="imgkl shadow">');
      $( "#owl-five .owl-nav .owl-next").html('<img src="{{ url("frontassets/img/r2.png") }}" height="45" style="margin-right:-10px;margin-top:150px;" height="55" class="imgkl shadow">');
   });
   
   
</script>
<style type="text/css">
   .imgkl{
   background-color: white;
   }
   .imgkl:hover
   {
   background: white !important;
   }
   .imgkl2{
   background-color: white;
   }
   .imgkl2:hover
   {
   background: white !important;
   }
</style>

<script type="text/javascript">
   $(".btn-group, .dropdown").hover(
                     function () {
                         $('>.dropdown-menu', this).stop(true, true).fadeIn("fast");
                         $(this).addClass('open');
                     },
                     function () {
                         $('>.dropdown-menu', this).stop(true, true).fadeOut("fast");
                         $(this).removeClass('open');
                     });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous"></script>
<script type="text/javascript">
   
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
   
    $("#submit-btn").click(function(e){
  
        e.preventDefault();
   
         var name = $("input[name=username]").val();
         var password = $("input[name=password]").val();
         var email = $("input[name=email]").val();
         var mobile = $("input[name=mobile]").val();
        $.ajax({
           type:'POST',
           url:"{{ route('register') }}",
           data:{name:name, password:password, email:email,mobile:mobile},
           success:function(data){
               if(data.success)
               {
                  $('#msgdetails').html('<div class="alert alert-success">'+data.success+'</div>');
               }else
               {
                  $('#msgdetails').html('<div class="alert alert-danger">'+data.error+'</div>');
               }
              
           }
        });
  
   });
   $("#login-btn").click(function(e){
  
        e.preventDefault();
   
         var mobile = $("#mobile").val();
         var password = $("#password").val();
      
        $.ajax({
           type:'POST',
           url:"{{ route('login') }}",
           data:{mobile:mobile, password:password},
           success:function(data){
            if(data.success)
            {
              $('#login').modal('hide');
              swal({
                title: "SUCCESS",
                text: data.success,
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
               
               }).then(function(){
                                      window.location.reload();
                                  })

            }else
            {
                $('#loginmsgdetails').html('<div class="alert alert-danger">'+data.error+'</div>');
            }

            }
  
   });
  });
   function getprice(the,count)
   {
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
   
      var weight = $(the).val();
      $.ajax({
            url: "{{ route('getpriceweight') }}",
            type:"POST",
            data:{weight: weight},
            success: function(result){
               var obj = JSON.parse(result);
               var urlimg = "{{ url("/") }}/"+obj.productimg;
               $('#productprice'+count).html('Rs.'+obj.productprice);
               $('#imgdata'+count).html(' <center><img src="'+urlimg+'" class="img-fluid sel_img23new" alt=""></center>');
               $('#varient_id').val(weight);
            }
      });
   }
   function  getpricetwo(the,count) {
     $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
   
      var weight = $(the).val();
      $.ajax({
            url: "{{ route('getpriceweight') }}",
            type:"POST",
            data:{weight: weight},
            success: function(result){
               var obj = JSON.parse(result);
               var urlimg = "{{ url("/") }}/"+obj.productimg;
               $('#productpricetwo'+count).html('Rs.'+obj.productprice);
               $('#imgdatatwo'+count).html(' <center><img src="'+urlimg+'" class="img-fluid sel_img23new" alt=""></center>');
               $('#varient_idtwo').val(weight);
            }
      });
   }
   function  getpricethree(the,count) {
       $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
   
      var weight = $(the).val();
      $.ajax({
            url: "{{ route('getpriceweight') }}",
            type:"POST",
            data:{weight: weight},
            success: function(result){
               var obj = JSON.parse(result);
               var urlimg = "{{ url("/") }}/"+obj.productimg;
               $('#productpricethree'+count).html('Rs.'+obj.productprice);
               $('#imgdatathree'+count).html(' <center><img src="'+urlimg+'" class="img-fluid sel_img23new" alt=""></center>');
               $('#varient_idthree').val(weight);
            }
      });
   }

   function getminus(main,count)
   {
      if(main == 1)
      {
         var data = $('.inner_two_spanone'+count).html();
         if(data > 0)
         {
            var minusdata = parseInt(data) - 1;
            $('.inner_two_spanone'+count).html(minusdata);
         }else 
         {
            $('.inner_two_spanone'+count).html(0);
         }
      }else if(main == 2)
      {
         var data = $('.inner_two_spantwo'+count).html();
         if(data > 0)
         {
            var minusdata = parseInt(data) - 1;
            $('.inner_two_spantwo'+count).html(minusdata);
         }else 
         {
            $('.inner_two_spantwo'+count).html(0);
         }
      }else
      {
         var data = $('.inner_two_spanthree'+count).html();
         if(data > 0)
         {
            var minusdata = parseInt(data) - 1;
            $('.inner_two_spanthree'+count).html(minusdata);
         }else 
         {
            $('.inner_two_spanthree'+count).html(0);
         }
      }   
      
      
     
   }
   function getplus(main,count)
   {
      if(main == 1)
      {
         var data = $('.inner_two_spanone'+count).html();
      
         var plusdata = parseInt(data) + 1;
         $('.inner_two_spanone'+count).html(plusdata);
      }else if(main == 2)
      {
         var data = $('.inner_two_spantwo'+count).html();
      
         var plusdata = parseInt(data) + 1;
         $('.inner_two_spantwo'+count).html(plusdata);
      }else 
      {
         var data = $('.inner_two_spanthree'+count).html();
      
         var plusdata = parseInt(data) + 1;
         $('.inner_two_spanthree'+count).html(plusdata);

      }
     
   }
   function addtobag(main,userid,pid,count)
   {
         if(userid == 0)
         {
            alert('Please Login !!!');
         }else
         {
            if(main == 1)
            {
               var qty = $('.inner_two_spanone'+count).html();
               var varient = $('#varient_id'+count).val();
            }else if(main == 2)
            {
               var qty = $('.inner_two_spantwo'+count).html();
               var varient = $('#varient_idtwo'+count).val();
            }else
            {
               var qty = $('.inner_two_spanthree'+count).html();
               var varient = $('#varient_idthree'+count).val();
            }
            
            
            
            $.ajaxSetup({
                     headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     }
            });
            $.ajax({
               url: "{{ route('addtobag') }}",
               type:"POST",
               data:{userid: userid,pid:pid,qty:qty,varient:varient},
               success: function(result){
                  if(result == 1)
                  {
                     alert('Product added to cart successfully');
                  }else if(result == 2){
                     alert('Product allready added to cart ');
                  }else
                  {
                     alert('Product not added to cart');
                  }

               }
            });
         }
         
   }
</script>