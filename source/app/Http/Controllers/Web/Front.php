<?php

namespace App\Http\Controllers\Web;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\Traits\SendSms;
use Session;

class Front extends BaseController
{
	use SendSms;
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function index(Request $request)
    {
        $current = Carbon::now();
       $lat = '28.573500';
       $lng = '77.323010';
        $cityname = 'Noida';
       $city = ucfirst($cityname);
       $nearbystore = DB::table('store')
                    ->select('del_range','store_id',DB::raw("6371 * acos(cos(radians(".$lat . ")) 
                    * cos(radians(store.lat)) 
                    * cos(radians(store.lng) - radians(" . $lng . ")) 
                    + sin(radians(" .$lat. ")) 
                    * sin(radians(store.lat))) AS distance"))
                  ->where('store.del_range','>=','distance')
                  ->orderBy('distance')
                  ->first();
if($nearbystore->del_range >= $nearbystore->distance) {               
      $topselling = DB::table('store_products')
                 ->join ('product_varient', 'store_products.varient_id', '=', 'product_varient.varient_id')
                  ->join ('product', 'product_varient.product_id', '=', 'product.product_id')
                  ->Leftjoin ('store_orders', 'store_products.varient_id', '=', 'store_orders.varient_id') 
                  ->Leftjoin ('orders', 'store_orders.order_cart_id', '=', 'orders.cart_id')
                  ->Leftjoin ('deal_product', 'product_varient.varient_id', '=', 'deal_product.varient_id')
                  ->select('store_products.store_id','store_products.stock','product_varient.varient_id','product.product_id','product.product_name', 'product.product_image', 'product_varient.description', 'store_products.price', 'store_products.mrp', 'product_varient.varient_image','product_varient.unit','product_varient.quantity','product.cat_id',DB::raw('count(store_orders.varient_id) as count'))
                  ->groupBy('store_products.store_id','store_products.stock','product_varient.varient_id','product.product_id','product.product_name', 'product.product_image', 'product_varient.description', 'store_products.price', 'store_products.mrp', 'product_varient.varient_image','product_varient.unit','product_varient.quantity')
                  ->where('store_products.store_id', $nearbystore->store_id)
                  ->where('deal_product.deal_price', NULL)
                  ->where('store_products.price','!=',NULL)
                  ->where('product.hide',0)
                  ->orderBy('count','desc')
                  ->limit(10)
                  ->get();
                  
        }


       
       $nearbystore2 = DB::table('store')
                    ->select('del_range','store_id',DB::raw("6371 * acos(cos(radians(".$lat . ")) 
                    * cos(radians(store.lat)) 
                    * cos(radians(store.lng) - radians(" . $lng . ")) 
                    + sin(radians(" .$lat. ")) 
                    * sin(radians(store.lat))) AS distance"))
                  ->where('store.del_range','>=','distance')
                  ->orderBy('distance')
                  ->first();
       if($nearbystore2->del_range >= $nearbystore2->distance) {               
      $new = DB::table('store_products')
                 ->join ('product_varient', 'store_products.varient_id', '=', 'product_varient.varient_id')
                  ->join ('product', 'product_varient.product_id', '=', 'product.product_id')
                  ->Leftjoin ('deal_product', 'product_varient.varient_id', '=', 'deal_product.varient_id')
                  ->select('store_products.store_id','store_products.stock','product_varient.varient_id','product.product_id','product.product_name', 'product.product_image', 'product_varient.description', 'store_products.price', 'store_products.mrp', 'product_varient.varient_image','product_varient.unit','product_varient.quantity','product.cat_id')
                  ->limit(10)
                   ->where('store_products.store_id', $nearbystore->store_id)
                  ->where('deal_product.deal_price', NULL)
                ->where('store_products.price','!=',NULL)
                ->where('product.hide',0)
                  ->orderByRaw('RAND()')
                  ->get();
              }
        $nearbystore3 = DB::table('store')
                    ->select('del_range','store_id',DB::raw("6371 * acos(cos(radians(".$lat . ")) 
                    * cos(radians(store.lat)) 
                    * cos(radians(store.lng) - radians(" . $lng . ")) 
                    + sin(radians(" .$lat. ")) 
                    * sin(radians(store.lat))) AS distance"))
                  ->where('store.del_range','>=','distance')
                  ->orderBy('distance')
                  ->first();
      if($nearbystore3->del_range >= $nearbystore3->distance) {               
      $recentselling = DB::table('store_products')
                 ->join ('product_varient', 'store_products.varient_id', '=', 'product_varient.varient_id')
                  ->join ('product', 'product_varient.product_id', '=', 'product.product_id')
                  ->Leftjoin ('store_orders', 'product_varient.varient_id', '=', 'store_orders.varient_id') 
                  ->Leftjoin ('orders', 'store_orders.order_cart_id', '=', 'orders.cart_id')
                  ->Leftjoin ('deal_product', 'product_varient.varient_id', '=', 'deal_product.varient_id')
                  ->select('store_products.store_id','store_products.stock','product_varient.varient_id','product.product_id','product.product_name', 'product.product_image', 'product_varient.description', 'store_products.price', 'store_products.mrp', 'product_varient.varient_image','product_varient.unit','product_varient.quantity','product.cat_id',DB::raw('count(store_orders.varient_id) as count'))
                  ->groupBy('store_products.store_id','store_products.stock','product_varient.varient_id','product.product_id','product.product_name', 'product.product_image', 'product_varient.description', 'store_products.price', 'store_products.mrp', 'product_varient.varient_image','product_varient.unit','product_varient.quantity')
                   ->where('store_products.store_id', $nearbystore->store_id)
                  ->orderByRaw('RAND()')
                  ->where('deal_product.deal_price', NULL)
                  ->where('product.hide',0)
                ->where('store_products.price','!=',NULL)
                  ->limit(10)
                  ->get();
              }
        $category = DB::table('categories')->where(array('level' => 0))->get();
        $banner = DB::table('banner')->get();
        return view('web.home',['banner' => $banner,'category' => $category ,'topselling' => $topselling,'newlyadded' => $new,'recentselling' => $recentselling]);
    }
    public function about()
    {
        $category = DB::table('categories')->where(array('level' => 0))->get();
        $about = DB::table('aboutuspage')->first();
        $banner = DB::table('banner')->get();
        return view('web.about',['banner' => $banner,'about' => $about,'category' => $category]);
    }
    public function privacy()
    {
        $category = DB::table('categories')->where(array('level' => 0))->get();
        $terms = DB::table('termspage')->first();

        return view('web.privacy',['terms' => $terms,'category' => $category]);
    }
   
    public function terms()
    {
        $terms = DB::table('termspage')->get();
        $category = DB::table('categories')->where(array('level' => 0))->get();
        
        return view('web.terms',['banner' => $banner,'terms' => $terms,'category' => $category]);
    }

    public function register(Request $request)
    {

    	$existmobile = DB::table('users')
    	    ->where('user_phone', $request->mobile)
    	    ->first();

    	if(!empty($existmobile))
    	{
    		return response()->json(['error'=>'User Allready Exists']);
    		
    	}else{
    		
    		$device_id = 'N/A';
    		$userimage = 'N/A';
    		$newarray = array(
    							'user_name'		=> $request->name,
    							'user_email'	=> $request->email,
    							'user_phone'	=> $request->mobile,
    							'user_image'	=> $userimage,
    							'user_password' => $request->password,
    							'device_id'		=> $device_id,
    							'reg_date'		=> date('Y-m-d H:i:s')
    						);
    		$insertdata = DB::table('users')->insertGetId($newarray);
    		if($insertdata)
    		{
    			DB::table('notificationby')
                            ->insert(['user_id'=> $insertdata,
                            'sms'=> '1',
                            'app'=> '1',
                            'email'=> '1']);
                            
                $smsby = DB::table('smsby')
              	->first();  
              	if($smsby->status== 1)
              	{        
	                $chars = "0123456789";
	                $otpval = "";
	                for ($i = 0; $i < 4; $i++){
	                    $otpval .= $chars[mt_rand(0, strlen($chars)-1)];
	                }
	                
	                
	                $otpmsg = $this->otpmsg($otpval,$request->mobile);
	                
	                $updateOtp = DB::table('users')
	                                ->where('user_phone', $request->mobile)
	                                ->update(['otp_value'=>$otpval]);
	                return response()->json(['success'=>'Registration Done Successfully.Please Verify Your Mobile']);
	            }else
	            {
	            	return response()->json(['success'=>'Registration Done Successfully']);
	            }
	            
    			// return redirect('/index')->with('success_message', 'User Regsiter Successfully');
    		}else
    		{
    			return response()->json(['error'=>'Something went wrong']);
    			
    		}
    	}
    }

    public function login(Request $request)
    {
    	$user_phone = $request->mobile;
    	$user_password = $request->password;
    	// $device_id = $request->device_id;
    	
    	$checkUserReg = DB::table('users')
    					->where('user_phone', $user_phone)
    					->first();
    					
    	if(!($checkUserReg) || $checkUserReg->is_verified== 0){
    		return response()->json(['error'=>'Phone not registered']);
    	  
    	}
                
    	$checkUser = DB::table('users')
    					->where('user_phone', $user_phone)
    					->where('user_password', $user_password)
    					->first();

    	if($checkUser){
    	    
    	    if($checkUser->is_verified == 0){
    	        $chars = "0123456789";
                $otpval = "";
                for ($i = 0; $i < 4; $i++){
                    $otpval .= $chars[mt_rand(0, strlen($chars)-1)];
                }
                
               $otpmsg = $this->otpmsg($otpval,$user_phone);
               
                $updateOtp = DB::table('users')
                                ->where('user_phone', $user_phone)
                                ->update(['otp_value'=>$otpval]);
                                
                $checkUser1 = DB::table('users')
            					->where('user_phone', $user_phone)
            					->first();                
                return response()->json(['success'=>'Verify Phone']);        
    	        
    	    }
    	   else{
    		   // $updateDeviceId = DB::table('users')
    		   //                      ->where('user_phone', $user_phone)
    		   //                      ->update(['device_id'=>$device_id]);
    		                       
    		   $checkUser1 = DB::table('users')
            					->where('user_phone', $user_phone)
            					->where('user_password', $user_password)
            					->first();
                $sessiondata = array(
                                        'userid' => $checkUser1->user_id,
                                        'name'   => $checkUser1->user_name,
                                        'email'  => $checkUser1->user_email
                                    );
    		    $request->session()->put($sessiondata); 
    		    return response()->json(['success'=>'login successfully']);                   
    			
    	   }	   
    	
    	}
    	else{
    		return response()->json(['error'=>'Wrong Password']);   
    		
    	}
    }

    public function logoutterm(Request $request)
    {
       
        $info = array(
                    'userid' =>'',
                    'name'    => '',
                    'email'   => '',
           );
        
        $request->session()->flush();
       return redirect()->route('index')->withSuccess("User logged out.");
        
    }
    public function product(Request $request)
    {

        $catdetails = DB::table('categories')->where(array('parent' => $request->segment(3)))->get();

        $finalarray = array();
        foreach($catdetails as $catdetails)
        {
            $product = DB::table('product')->where(array('cat_id' => $catdetails->cat_id))->get();
            
            foreach($product as $product)
            {
                $infodata = $product;

                array_push($finalarray,$infodata);
            }
        }
        
 
        $category = DB::table('categories')->where(array('level' => 0))->get();
        return view('web.product',['category' => $category ,'product' => $finalarray ,'cat_id_url' => $request->segment(3)]);
    }

    public function getpriceweight(Request $request)
    {
        $productvarient = DB::table('product_varient')->where(array('varient_id' => $request->weight ))->first();

        $productprice = $productvarient->base_price;
        $productarray = array('productprice' => $productprice,'productimg' => $productvarient->varient_image);
       
        echo json_encode($productarray);
    }
    public function getproductlist(Request $request)
    {
        $catdetails = DB::table('categories')->where(array('parent' => $request->cat_id))->get();

        $finalarray = array();
        foreach($catdetails as $catdetails)
        {
            $product = DB::table('product')->where(array('cat_id' => $catdetails->cat_id))->get();
            
            foreach($product as $product)
            {
                $infodata = $product;

                array_push($finalarray,$infodata);
            }
        }
        $productwight = '';
        if(count($finalarray) > 0)
        {
                $html    = '<div class="row">';
                            $i= 1; foreach($finalarray as $product) {
                              $productvarientwigth = DB::table('product_varient')->where(array('product_id' => $product->product_id))->get();
                              $productvarient = DB::table('product_varient')->where(array('product_id' => $product->product_id))->first();

                             $urldata = url('/').'/'.$product->product_image;
                              
                $html .=     '<div class="col-lg-4 col-md-6 " >
                              <!-- <a href="#"> -->
                              <div class="member pro_mob29" stytle="">
                                 <div class="member-img" id="imgdata<?php echo $i;?>">
                                    <center><a href="{{route("productdetail",'.$product->cat_id.')}}"><img src="'.$urldata.'" class="img-fluid sel_img23new" alt=""></a></center>
                                 </div>
                                 <div class="member-info">
                                    <h4>'.$product->product_name.'</h4>
                                    <select class="select_gram" onchange="getprice(this,'.$i.')">';
                                       foreach($productvarientwigth as $productvarientwigth){
                            $html .=       '<option value="'.$productvarientwigth->varient_id.'" >'.$productvarientwigth->unit.'</option>';
                                       }
                            $html .=   '</select>
                                    <h4 class="h4_rupee" id="productprice'.$i.'">Rs. '.$productvarient->base_price.'</h4>
                                    <div class="btn-group btn-group-sm outer_but772" role="group" aria-label="...">
                                       <button class="plus-one inner_one'.$i.'" onclick="getminus('.$i.')"><i class="fas fa-minus inner_one_i2"></i></button>
                                       <button class="plus-middle inner_two'.$i.'"><span class="inner_two_span'.$i.'">1</span></button>
                                       <button class="plus-two inner_three'.$i.'" onclick="getplus('.$i.')"><i class="fas fa-plus inner_three_i2"></i></button>
                                    </div>
                                    <button class="add_but">Add to bag</button>
                                 </div>
                              </div>
                             <!--  </a> -->
                           </div>';

                           $i++; }
                        $html .= '</div>';
                
                        echo $html;
        }else
        {
            echo '<center><h3 style="color:red;">Data Not Found</h3></center>';
        }
    }
    public function productdetail(Request $request)
    {
        
        $catdetails = DB::table('categories')->where(array('parent' => $request->segment(3)))->get();

        $finalarray = array();
        foreach($catdetails as $catdetails)
        {
            $product = DB::table('product')->where(array('cat_id' => $catdetails->cat_id))->get();
            
            foreach($product as $product)
            {
                $infodata = $product;

                array_push($finalarray,$infodata);
            }
        }
        $category = DB::table('categories')->where(array('level' => 0))->get();

        $productdetails = DB::table('product')->where(array('cat_id' => $request->segment(3)))->get();

        return view('web.product_detail',['category' => $category,'product' =>$finalarray,'productdetails' => $productdetails]);
    }
    public function getproductdetailschange(Request $request)
    {
        
        $varienttdetails = DB::table('product_varient')->where(array('varient_id' => $request->varientid))->first();
           $array = array('price' => $varienttdetails->base_price,'pid' => $varienttdetails->product_id);
            echo json_encode($array);

    }
    public function getproductdata(Request $request)
    {
        $product = DB::table('product')->where(array('product_id' => $request->pid))->first();
        $varienttdetails = DB::table('product_varient')->where(array('product_id' => $request->pid))->get();
        $singlevarienttdetails =   DB::table('product_varient')->where(array('product_id' => $request->pid))->first();
        $html = '';   
        $html .=   '<h3>'.$product->product_name.'<span style="float: right;font-size: 15px;position: relative;top: 5px; right: 100px;"><i class="fas fa-star" style="color: #37A235"></i> <i class="fas fa-star" style="color: #37A235"></i> <i class="fas fa-star" style="color: #37A235"></i> <i class="far fa-star"></i> <i class="far fa-star"></i></span></h3>
                  <p style="margin-top: 19px;font-size: 15px;"><b>Select Size:</b></p>
                  <select class="select_gram23 shadow-sm" onchange="changeproductdeatils(this)">';
                    foreach($varienttdetails as $varienttdetails) {
                $html .=   '<option value="'.$varienttdetails->varient_id.'">'.$varienttdetails->quantity.''.$varienttdetails->unit.'</option>';

                    }
            $html .= '</select>
                  <h3 style="margin-top: 25px;"><b ><span id="pricedata">Rs.'.$singlevarienttdetails->base_price.'</span></b></h3>
                  <p style="margin-bottom: 10px;font-size: 14px;margin-top: -6px;color: #37A235;"><!-- Extra ₹60 discount --></p>
                  <div class="btn-group btn-group-sm outer_but772new shadow"  style="border-radius: 30px;" role="group" aria-label="...">
                    <input type="hidden" name="piddata" id="piddata" value="'.$product->product_id.'">
                    <input type="hidden" name="varientiddata" id="varientiddata" value="'.$singlevarienttdetails->varient_id.'">
                     <button class="inner_one2new" onclick="getminus()"><i class="fas fa-minus inner_one_i2new"></i></button>
                     <button class="inner_two2new"><span class="inner_two_span2new">1</span></button>
                     <button class="inner_three2new" onclick="getplus()"><i class="fas fa-plus inner_three_i2new"></i></button>
                  </div>';
                  if(session('userid') != '') { 
                $html .= '<button class="add_butnew shadow" onclick="addtobag('.session('userid').')">Add to bag</button>';
                  }else {
                $html .=    '<button class="add_butnew shadow" onclick="addtobag(0)">Add to bag</button>';
                        
                   }
                $html .=  '<h5 style="margin-top: 42px;">Product Description :</h5>
                  <p style="width: 90%;font-size: 13px;margin-top: 13px;color: #A6A6A6">'.$singlevarienttdetails->description.'</p>';
        echo $html;
    }
    public function addtobag(Request $request)
    { 
        $exist = DB::table('cart')->where(array('product_id' => $request->pid,'user_id' => $request->userid,'varient_id' =>  $request->varient))->first();

            
        if(!empty($exist->cart_id))
        {
            echo 2;
        }else
        {
            $insertarray =  array(
                            'product_id' => $request->pid,
                            'user_id'    => $request->userid,
                            'qty'        => $request->qty,
                            'varient_id' => $request->varient
                        );
            $insert = DB::table('cart')->insert($insertarray);

            if($insert == 1)
            {
                echo 1;
            }else
            { 
                echo 0;
            }
        }
        
    }
    public function getsubcat(Request $request)
    {
        $subdata = DB::table('categories')->where(array('parent' => $request->catid))->get();
        $html = '';
        foreach($subdata as $subdata)
        {
            $html .= '<li><a href="#" onclick="geturlproduct('.$subdata->cat_id.')">'.$subdata->title.'</li>';
        }
        echo $html;
    }
    public function updateqtycart(Request $request)
    {
        $updateqty = DB::table('cart')->where(array('cart_id' => $request->cartid))->update(array('qty' => $request->data));
        if($updateqty == 1)
        {
            echo 1;
        }else
        {
            echo 0;
        }
    }
    public function removecartitem(Request $request)
    {
        $cartremove = DB::table('cart')->where(array('cart_id' => $request->cartid))->delete();
        if($cartremove == 1)
        {
            echo 1;

        }else
        {
            echo 0;
        }
    }

    public function cart(Request $request)
    {
      $category = DB::table('categories')->where(array('level' => 0))->get();
      $where= array('user_id'=> session('userid'));
      $cartdata = getTableWhere('cart',$where);

      
        return view('web.cart',['category' => $category,'cartitem' => $cartdata]);
    }
    public function checkout(Request $request)
    {

      $where= array('user_id'=> session('userid'));
      $cartdata = getTableWhere('cart',$where);


        $category = DB::table('categories')->where(array('level' => 0))->get();
        $deliverycart = DB::table('freedeliverycart')->first();
        return view('web.checkout',['category' => $category,'cartitem' =>$cartdata,'delivery' => $deliverycart]);
    }
    public function profile(Request $request)
    {
      $profile = DB::table('users')->where(array('user_id' => session('userid')))->first();
      $orders = DB::table('orders')->where(array('user_id' => session('userid')))->get();
      $category = DB::table('categories')->where(array('level' => 0))->get();

      return view('web.profile',['category' => $category,'profile' => $profile,'orders' => $orders,'activeorder' => $orders]);
    }
    public function updateprofile(Request $request)
    {

        $info = array(
                      'user_name' => $request->username,
                      'user_email' => $request->useremail,
                      'user_phone' => $request->mobilenumber,
                    );
        $update = DB::table('users')->update($info);
        if($update == 1)
        {
          echo '<script>alert("Profile Updated Successfully")</script>';
        }
        $profile = DB::table('users')->where(array('user_id' => session('userid')))->first();
      $orders = DB::table('orders')->where(array('user_id' => session('userid')))->get();
      $category = DB::table('categories')->where(array('level' => 0))->get();
        return view('web.profile',['category' => $category,'profile' => $profile,'orders' => $orders,'activeorder' => $orders]);

    }
    public function placeorder(Request $request)
    {
        if($request->deliverycharges == 'Free')
        {
            $delcharges = 0;
        }else
        {
            $delcharges = $request->deliverycharges;
        }
        $info = array(
                'user_id' => session('userid'),
                'store_id' => $request->storeid,
                'address_id' => $request->addressid,
                'total_price' => $request->finalamount,
                'price_without_delivery' => $request->subtotal,
                'payment_method' => $request->paymentmethod,
                'order_date'     => date('Y-m-d'),
                'delivery_charge' => $delcharges,
                'cart_id'         => 0,
                'total_products_mrp' => 0,
                'paid_by_wallet'    => 0,
                'rem_price'         => 0,
                'delivery_date'     => date('Y-m-d'),
                'time_slot'         => '',
                'dboy_id'           => 0,
                'order_status'      =>'Pending',
                'coupon_id'         => 0,
                'coupon_discount'   => 0,
                'cancel_by_store'   => 0,
        );
        $insert = DB::table('orders')->insert($info);
        if($insert != 0)
        {
          
          echo $insert;
        }
    }
}
