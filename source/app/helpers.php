<?php

 function getTableWhere($table,$where) {

        $data = \DB::table($table)
            ->select(\DB::raw('*'))
            ->where($where)
            ->get();

        return $data;
    }

?>